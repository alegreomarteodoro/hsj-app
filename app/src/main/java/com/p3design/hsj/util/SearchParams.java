package com.p3design.hsj.util;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.p3design.hsj.observer.SearchParamObserver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class SearchParams {

    private SearchParamObserver observer;

    public static final String QUERY_STRING_KEY = "search";
    public static final String COUNTRY_KEY = "pais";
    public static final String STATE_KEY = "provincia";
    public static final String CITY_KEY = "localidad";
    public static final String NEIGHBORHOOD_KEY = "barrio";
    public static final String PLAYERS_KEY = "jugadores";
    public static final String SOIL_TYPES_KEY = "tipo_suelo";
    public static final String INFRAESTRUCTURE_KEY = "techo";
    public static final String LOCATION_KEY = "coordenadas";
    public static final String DISTANCE_KEY = "distancia";

    public static final String DEFAULT_DISTANCE_VALUE = "20";


    Map<String, String> filters = new HashMap<String, String>();

    public SearchParams(SearchParamObserver observer) {
        this.observer = observer;
    }

    public SearchParams() {

    }

    public SearchParamObserver getObserver() {
        return observer;
    }

    public void setObserver(SearchParamObserver observer) {
        this.observer = observer;
    }

    public void addParam(String key, Object object, String visibleValue) {
        //String param = "/" + key + "/";
        String value = "";

        if (key.equals(LOCATION_KEY)) {

            if (object instanceof LatLng) {

                LatLng latLng = (LatLng) object;
                value += latLng.latitude + "," + latLng.longitude;

            } else if (object instanceof String) {

                value += object;

            } else if (object instanceof Location) {

                Location location = (Location) object;
                value += location.getLatitude() + "," + location.getLongitude();

            }

            filters.put(DISTANCE_KEY, DEFAULT_DISTANCE_VALUE);

        } else {

            if (object instanceof String) {

                value += object;

            } else if (object instanceof Integer) {

                value += String.valueOf(object);

            }

        }

        if(filters.containsKey(key)) {
            removeParam(key);
        }

        filters.put(key, value);

        if(observer != null) observer.onParamAdded(key, object, visibleValue);
    }


    public void removeParam(String key) {
        filters.remove(key);

        if (key.equals(LOCATION_KEY)) {
            filters.remove(DISTANCE_KEY);
        }

        if(observer != null) observer.onParamRemoved(key);
    }

    public void removeAllParams() {
        for(Map.Entry<String, String> filterItem: getFilters().entrySet()) {
            if(observer != null) observer.onParamRemoved(filterItem.getKey());
        }
        filters.clear();
    }

    public Map<String, String> getFilters() {
        return filters;
    }


    public String toString() {
        String resultQueryString = "";

        for(Map.Entry<String, String> filterItem: getFilters().entrySet()) {
            resultQueryString += "/" + filterItem.getKey() + "/" + filterItem.getValue();
        }

        return resultQueryString;
    }
}