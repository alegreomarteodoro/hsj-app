package com.p3design.hsj.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.p3design.hsj.app.App;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 5/9/15.
 */
public class DynamicView {

    ViewType mType;
    WindowLayoutType mWindowLayoutType;
    Context mContext;
    LinearLayout mLinearLayoutContainer;
    RelativeLayout mRelativeLayoutContainer;
    LinearLayout mWindowContainer;
    private LayoutInflater mLayoutInflater;
    private Map<String, View> mDefaultViews = new HashMap<String, View>();

    public enum VisibilityType {
        SINGLE,
        MULTIPLE
    }

    public enum ViewType {
        ACTIVITY,
        FRAGMENT
    }

    public enum WindowLayoutType {
        RELATIVE_LAYOUT,
        LINEAR_LAYOUT
    }

    public DynamicView(Context context, ViewType type, WindowLayoutType windowLayoutType) {
        mContext = context;
        mLayoutInflater = ((Activity) context).getLayoutInflater();
        mType = type;
        mWindowLayoutType = windowLayoutType;

        initLayoutContainer(context);
        setDefaultViews();
    }

    public DynamicView(Context context, ViewType type) {
        this(context, type, WindowLayoutType.LINEAR_LAYOUT);
    }

    public DynamicView(Context context, View currentView) {
        this(context, ViewType.ACTIVITY);

        addView(App.MAIN_LAYOUT, currentView);
        showView(App.MAIN_LAYOUT);
    }

    public DynamicView(Context context, int currentLayoutId) {
        this(context, ViewType.ACTIVITY);

        View currentView = mLayoutInflater.inflate(currentLayoutId, null);
        addView(App.MAIN_LAYOUT, currentView);
        showView(App.MAIN_LAYOUT);
    }

    public DynamicView(Context context, int currentLayoutId, ViewType type) {
        this(context, type);

        View currentView = mLayoutInflater.inflate(currentLayoutId, null);
        addView(App.MAIN_LAYOUT, currentView);
        showView(App.MAIN_LAYOUT);
    }

    public DynamicView(Context context, int currentLayoutId, WindowLayoutType windowLayoutType) {
        mContext = context;
        mLayoutInflater = ((Activity) context).getLayoutInflater();
        mType = ViewType.ACTIVITY;
        mWindowLayoutType = windowLayoutType;

        initLayoutContainer(context);
        setDefaultViews();

        View currentView = mLayoutInflater.inflate(currentLayoutId, null);
        addView(App.MAIN_LAYOUT, currentView);
        showView(App.MAIN_LAYOUT);
    }

    private void initLayoutContainer(Context context) {
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        mWindowContainer = new LinearLayout(context);
        mWindowContainer.setOrientation(LinearLayout.VERTICAL);
        mWindowContainer.setLayoutParams(linearLayoutParams);

        if(mWindowLayoutType == WindowLayoutType.LINEAR_LAYOUT) {

            mLinearLayoutContainer = new LinearLayout(context);
            mLinearLayoutContainer.setOrientation(LinearLayout.VERTICAL);
            mLinearLayoutContainer.setLayoutParams(linearLayoutParams);

            mWindowContainer.addView(mLinearLayoutContainer);

        } else if (mWindowLayoutType == WindowLayoutType.RELATIVE_LAYOUT) {

            mRelativeLayoutContainer = new RelativeLayout(context);
            mRelativeLayoutContainer.setLayoutParams(relativeLayoutParams);

            mWindowContainer.addView(mRelativeLayoutContainer);
        }

        if(mType == ViewType.FRAGMENT) {

        } else if (this.mContext != null && this.mContext instanceof Activity) {
            ((Activity) this.mContext).setContentView(this.mWindowContainer);
        }
    }

    private void setDefaultViews() {
        //addViewByLayoutId(INTERNET_OFF_LAYOUT, R.layout.fragment_internet_off);
        //addViewByLayoutId(OTHER_ERROR_LAYOUT, R.layout.fragment_other_error);
    }

    public View addView(String key, View view) {
        view.setVisibility(View.GONE);
        mDefaultViews.put(key, view);
        if(mLinearLayoutContainer != null) {
            mLinearLayoutContainer.addView(view);
        } else if (mRelativeLayoutContainer != null) {
            mRelativeLayoutContainer.addView(view);
        }
        return view;
    }

    public View addMainView(View view) {
        addView(App.MAIN_LAYOUT, view);
        showView(App.MAIN_LAYOUT);
        return view;
    }

    public View addViewByLayoutId(String key, int layoutId) {
        View view = this.mLayoutInflater.inflate(layoutId, null);
        addView(key, view);
        return view;
    }

    public View addMainViewByLayoutId(int layoutId) {
        View view = addViewByLayoutId(App.MAIN_LAYOUT, layoutId);
        showView(App.MAIN_LAYOUT);
        return view;
    }

    public View getMainView() {
        return getView(App.MAIN_LAYOUT);
    }

    public View getView(String key) {
        return mDefaultViews.get(key);
    }

    public void showView(String key, VisibilityType visibilityType) {
        if(visibilityType == VisibilityType.SINGLE) {
            for (Map.Entry<String, View> mapItem : mDefaultViews.entrySet()) {
                mapItem.getValue().setVisibility(View.GONE);
            }
        }
        mDefaultViews.get(key).setVisibility(View.VISIBLE);
    }

    public void showView(String key) {
        showView(key, VisibilityType.SINGLE);
    }

    public void hideView(String key) {
        mDefaultViews.get(key).setVisibility(View.GONE);
    }

    public LinearLayout getRootView() {
        return mWindowContainer;
    }
}