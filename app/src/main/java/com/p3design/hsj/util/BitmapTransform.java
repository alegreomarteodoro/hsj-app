package com.p3design.hsj.util;

import android.graphics.Bitmap;
//

/**
 * Created by omarteodoroalegre on 4/3/16.
 */
public class BitmapTransform { //implements Transformation {

    public static final int MAX_WIDTH = 70;
    public static final int MAX_HEIGHT = 70;

    int maxWidth;
    int maxHeight;

    public BitmapTransform(int maxWidth, int maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public BitmapTransform() {
        this(MAX_WIDTH, MAX_HEIGHT);
    }

    //@Override
    public Bitmap transform(Bitmap source) {
        int targetWidth, targetHeight;
        double aspectRatio;

        if (source.getWidth() > source.getHeight()) {
            targetWidth = maxWidth;
            aspectRatio = (double) source.getHeight() / (double) source.getWidth();
            targetHeight = (int) (targetWidth * aspectRatio);
        } else {
            targetHeight = maxHeight;
            aspectRatio = (double) source.getWidth() / (double) source.getHeight();
            targetWidth = (int) (targetHeight * aspectRatio);
        }

        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
        if (result != source) {
            source.recycle();
        }
        return result;
    }

    //@Override
    public String key() {
        return maxWidth + "x" + maxHeight;
    }

    public static int getDefaultSize() {
        return (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
    }
};