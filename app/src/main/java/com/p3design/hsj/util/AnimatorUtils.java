package com.p3design.hsj.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.p3design.hsj.R;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class AnimatorUtils {
    public static void out(final View myView) {
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = (myView.getTop() + myView.getBottom()) / 2;

        int initialRadius = myView.getWidth();

        android.animation.Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(android.animation.Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.INVISIBLE);
            }
        });

        anim.start();
    }

    public static void in(View myView) {
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = (myView.getTop() + myView.getBottom()) / 2;
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight());
        android.animation.Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    public static void translate(View view, Direction direction) {
        int defaultTranslation = 300;
        int defaultDuration = 1000;

        if(direction == Direction.UP || direction == Direction.DOWN) {
            ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(view, "translationY", direction == Direction.UP ? defaultTranslation : -defaultTranslation, 0);
            animatorTranslateY.setDuration(defaultDuration);
            animatorTranslateY.start();
        } else {
            ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(view, "translationX", direction==Direction.RIGHT ? defaultTranslation : -defaultTranslation, 0);
            animatorTranslateX.setDuration(defaultDuration);
            animatorTranslateX.start();
        }
    }

    public static void translate(View view) {
        translate(view, Direction.UP);
    }

    public static void heartBeat(View view) {
        Animation pulse = AnimationUtils.loadAnimation(view.getContext(), R.anim.heart_beat);
        view.startAnimation(pulse);
    }

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
