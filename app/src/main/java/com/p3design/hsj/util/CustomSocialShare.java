package com.p3design.hsj.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.p3design.hsj.app.App;

/**
 * Created by omarteodoroalegre on 6/3/16.
 */
public class CustomSocialShare {

    public enum Sites {
        FACEBOOK,
        TWITTER
    }

    public static void inFabebook(Context context, String url) {
        share(context, url, Sites.FACEBOOK);
    }

    public static void inTwitter(Context context, String url) {
        share(context, url, Sites.TWITTER);
    }

    public static void share(Context context, String url, Sites site) {

        String urlShare = "";

        if(site == Sites.FACEBOOK) {
            urlShare = "https://www.facebook.com/sharer/sharer.php?u=" +  url;
        } else if(site == Sites.TWITTER) {
            urlShare = "https://twitter.com/intent/tweet?text=" + url;
        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(urlShare));
        context.startActivity(i);
    }
}
