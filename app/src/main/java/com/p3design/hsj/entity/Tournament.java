package com.p3design.hsj.entity;

import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 15/7/15.
 */
public class Tournament extends Entity {

    protected Integer positiveVotes = 0; //votos_positivos
    protected Country country; //pais
    protected State state; //provincia
    protected City city; //localidad
    protected Neighborhood neighborhood; //barrio
    protected String web; //web
    protected String phone; //telefono
    protected Image featuredImage; //imagen_destacado
    protected Image image; //imagen
    protected String tags;
    protected String description;
    protected ArrayList<Feature> features = new ArrayList<Feature>();
    protected ArrayList<Headquarter> hosts = new ArrayList<Headquarter>();
    protected String tournamentTypes;
    protected double distance = 0;
    protected String mail;

    public Integer getPositiveVotes() {
        return positiveVotes;
    }

    public void setPositiveVotes(Integer positiveVotes) {
        this.positiveVotes = positiveVotes;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Neighborhood getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(Neighborhood neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Image getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(Image featuredImage) {
        this.featuredImage = featuredImage;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }

    public void addFeature(Feature feature) {
        this.features.add(feature);
    }

    public ArrayList<Headquarter> getHosts() {
        return hosts;
    }

    public void setHosts(ArrayList<Headquarter> hosts) {
        this.hosts = hosts;
    }

    public void addHost(Headquarter headquarter) {
        this.hosts.add(headquarter);
    }

    public String getTournamentTypes() {
        return tournamentTypes;
    }

    public void setTournamentTypes(String tournamentTypes) {
        this.tournamentTypes = tournamentTypes;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
