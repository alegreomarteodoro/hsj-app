package com.p3design.hsj.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by omarteodoroalegre on 12/2/16.
 */
public class ContactInfoItem {
    protected String primaryTitle;
    protected String secondaryTitle;
    protected Drawable primaryIcon;
    protected Drawable secondaryIcon;
    protected ItemType type;
    protected boolean showDivider = false;
    protected Entity entity;

    public String getPrimaryTitle() {
        return primaryTitle;
    }

    public void setPrimaryTitle(String primaryTitle) {
        this.primaryTitle = primaryTitle;
    }

    public String getSecondaryTitle() {
        return secondaryTitle;
    }

    public void setSecondaryTitle(String secondaryTitle) {
        this.secondaryTitle = secondaryTitle;
    }

    public Drawable getPrimaryIcon() {
        return primaryIcon;
    }

    public void setPrimaryIcon(Drawable primaryIcon) {
        this.primaryIcon = primaryIcon;
    }

    public Drawable getSecondaryIcon() {
        return secondaryIcon;
    }

    public void setSecondaryIcon(Drawable secondaryIcon) {
        this.secondaryIcon = secondaryIcon;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public void setShowDivider(boolean value) {
        this.showDivider = value;
    }

    public boolean getShowDivider() {
        return showDivider;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public enum ItemType {
        PHONE,
        ADDRESS,
        CONTACT_INFO
    }
}
