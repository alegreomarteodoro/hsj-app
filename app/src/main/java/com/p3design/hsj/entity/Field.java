package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 2/2/16.
 */
public class Field extends Entity{
    Player player; //jugadores
    Double price; //precio
    Boolean ceiling; //techo
    Boolean light; //luz
    SoilType soilType; //tipo de suelo

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getCeiling() {
        return ceiling;
    }

    public void setCeiling(Boolean ceiling) {
        this.ceiling = ceiling;
    }

    public Boolean getLight() {
        return light;
    }

    public void setLight(Boolean light) {
        this.light = light;
    }

    public SoilType getSoilType() {
        return soilType;
    }

    public void setSoilType(SoilType soilType) {
        this.soilType = soilType;
    }
}
