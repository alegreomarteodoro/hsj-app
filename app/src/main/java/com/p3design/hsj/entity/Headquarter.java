package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 30/8/15.
 */
public class Headquarter extends Entity {
    Playfield playfield;
    String description;
    String neighborhood;

    public Playfield getPlayfield() {
        return playfield;
    }

    public void setPlayfield(Playfield playfield) {
        this.playfield = playfield;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }
}
