package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 4/7/15.
 */
public class State extends Entity{

    private Integer countryId;

    public State() { }

    public State(int id, String title) {
        super(id, title);
    }

    public Integer getCountry() {
        return countryId;
    }

    public void setCountry(Integer countryId) {
        this.countryId = countryId;
    }
}
