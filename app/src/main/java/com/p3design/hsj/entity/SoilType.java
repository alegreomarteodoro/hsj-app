package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class SoilType extends Entity {

    public SoilType() { }

    public SoilType(int id, String title) {
        super(id, title);
    }
}
