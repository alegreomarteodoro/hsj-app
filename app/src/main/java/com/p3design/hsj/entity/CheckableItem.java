package com.p3design.hsj.entity;

import android.widget.CheckBox;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class CheckableItem {

    protected int id;
    protected String title;
    protected String subtitle;
    protected boolean checked;
    protected CheckableItem.ItemType itemType = ItemType.DEFAULT;
    protected CheckBox checkBox;

    public CheckableItem(int id, String title, String subtitle, boolean checked) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.checked = checked;
    }

    public CheckableItem(int id, String title, boolean checked) {
        this(id, title, null, checked);
    }

    public CheckableItem(int id, String title, CheckableItem.ItemType itemType) {
        this(id, title);
        this.itemType = itemType;
    }

    public CheckableItem(int id, String title) {
        this(id, title, false);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public void setSubtitle(String subtitle)
    {
        this.subtitle = subtitle;
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }

    public enum ItemType {
        PLAYER,
        SOIL_TYPE,
        INFRAESTRUCTURE,
        DEFAULT
    }
}
