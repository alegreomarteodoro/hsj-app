package com.p3design.hsj.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 1/9/15.
 */
public class Gallery implements Serializable {
    protected ArrayList<Image> images = new ArrayList<Image>();

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }
}
