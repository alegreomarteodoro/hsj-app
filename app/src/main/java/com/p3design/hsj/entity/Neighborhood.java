package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 4/7/15.
 */
public class Neighborhood extends Entity{

    private Integer cityId;

    public Neighborhood() { }

    public Neighborhood(int id, String title) {
        super(id, title);
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }
}
