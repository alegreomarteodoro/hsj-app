package com.p3design.hsj.entity;

import java.io.Serializable;

/**
 * Created by omarteodoroalegre on 1/9/15.
 */
public class Image implements Serializable {
    protected String title;
    protected String subtitle;
    protected String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
