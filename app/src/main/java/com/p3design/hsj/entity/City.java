package com.p3design.hsj.entity;

/**
 * Created by omarteodoroalegre on 4/7/15.
 */
public class City extends Entity{

    private Integer stateId;

    public City() {}

    public City(int id, String title) {
        super(id, title);
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }
}
