package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Article;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 14/7/15.
 */
public class ArticleService extends Service{
    public static final String SERVICE_URL = DEFAULT_URL + "/articles"; //Noticias

    public ArticleService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> articles = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonObject = (JSONObject) items.get(i);

                Article item = new Article();
                item.setId(jsonObject.getInt("cgblog_id"));
                item.setTitle(jsonObject.getString("cgblog_title"));
                item.setSummary(jsonObject.getString("summary"));
                item.setContent(jsonObject.getString("cgblog_data"));
                item.setDetailUrl(jsonObject.getString("detail_url"));
                item.setFeatured(jsonObject.getBoolean("featured"));
                item.setCreatedAt(jsonObject.getString("cgblog_date"));

                Image image = new Image();
                Image featuredImage = new Image();
                if(jsonObject.has("image") && !jsonObject.isNull("image")) {
                    image.setUrl(jsonObject.getString("image"));
                }
                if(jsonObject.has("featured_image") && !jsonObject.isNull("featured_image")) {
                    featuredImage.setUrl(jsonObject.getString("featured_image"));
                }
                featuredImage.setSubtitle(jsonObject.getString("cgblog_title"));
                image.setSubtitle(jsonObject.getString("cgblog_title"));

                JSONArray categories = jsonObject.getJSONArray("categories");
                if(categories.length() > 0) {
                    item.setCategory(((JSONObject) categories.get(0)).getString("name"));
                    image.setTitle(item.getCategory());
                    featuredImage.setTitle(item.getCategory());
                }

                item.setFeaturedImage(featuredImage);
                item.setImage(image);

                articles.add(item);
            }

            return articles;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Noticias...");
    }
}
