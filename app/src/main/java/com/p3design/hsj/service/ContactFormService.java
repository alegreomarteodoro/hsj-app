package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.State;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 3/3/16.
 */
public class ContactFormService extends Service{

    public static final String SERVICE_URL = DEFAULT_URL + "/contact-form";

    public ContactFormService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");
            return new ArrayList<Entity>();
        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Enviando Formulario...");
    }

}
