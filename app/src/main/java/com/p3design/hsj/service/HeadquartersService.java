package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by omarteodoroalegre on 30/8/15.
 */
public class HeadquartersService extends Service {

    public static final String SERVICE_URL = DEFAULT_URL + "/headquarters"; //Sedes

    public HeadquartersService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    @Override
    public List<Entity> parseJsonObject(JSONObject response) {
        return null;
    }
}
