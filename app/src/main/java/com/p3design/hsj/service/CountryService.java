package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Country;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class CountryService extends Service{

    public static final String SERVICE_URL = DEFAULT_URL + "/countries";

    public CountryService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> countries = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonCountryObj = (JSONObject) items.get(i);

                Country item = new Country();
                item.setId(jsonCountryObj.getInt("id"));
                item.setTitle(jsonCountryObj.getString("title"));

                countries.add(item);
            }

            return countries;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Paises...");
    }
}
