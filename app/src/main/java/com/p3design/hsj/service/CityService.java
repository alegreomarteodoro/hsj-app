package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.City;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class CityService extends Service{
    public static final String SERVICE_URL = DEFAULT_URL + "/cities";

    public CityService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> cities = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonCityObj = (JSONObject) items.get(i);

                City item = new City();
                item.setId(jsonCityObj.getInt("id"));
                item.setTitle(jsonCityObj.getString("title"));
                item.setStateId(jsonCityObj.getInt("provincia"));

                cities.add(item);
            }

            return cities;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Localidades...");
    }
}
