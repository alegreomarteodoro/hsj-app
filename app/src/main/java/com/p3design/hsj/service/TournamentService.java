package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Tournament;
import com.p3design.hsj.entity.City;
import com.p3design.hsj.entity.Country;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Feature;
import com.p3design.hsj.entity.Headquarter;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Neighborhood;
import com.p3design.hsj.entity.State;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 14/7/15.
 */
public class TournamentService extends Service{

    public static final String SERVICE_URL = DEFAULT_URL + "/championships"; //Torneos

    public TournamentService(String id, Context context, ServiceObserver observer) {
        super(id, context, observer);
        setServiceUrl(SERVICE_URL);
    }
    @Override
    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> championships = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonObject = (JSONObject) items.get(i);

                Tournament item = new Tournament();

                if(jsonObject.has("id") && !jsonObject.isNull("id")) {
                    item.setId(jsonObject.getInt("id"));
                }

                if(jsonObject.has("title") && !jsonObject.isNull("title")) {
                    item.setTitle(jsonObject.getString("title"));
                }

                if(jsonObject.has("detail_url") && !jsonObject.isNull("detail_url")) {
                    item.setDetailUrl(jsonObject.getString("detail_url"));
                }

                if(jsonObject.has("email") && !jsonObject.isNull("email")) {
                    item.setMail(jsonObject.getString("email"));
                }

                if(jsonObject.has("imagen") && !jsonObject.isNull("imagen")) {
                    Image featuredImage = new Image();
                    featuredImage.setUrl(jsonObject.getString("imagen"));
                    featuredImage.setTitle(item.getTitle());
                    item.setFeaturedImage(featuredImage);
                }

                if(jsonObject.has("votos_positivos") && !jsonObject.isNull("votos_positivos")) {
                    item.setPositiveVotes(jsonObject.getInt("votos_positivos"));
                }

                if(jsonObject.has("telefono") && !jsonObject.isNull("telefono")) {
                    item.setPhone(jsonObject.getString("telefono"));
                }

                if(jsonObject.has("web") && !jsonObject.isNull("web")) {
                    item.setWeb(jsonObject.getString("web"));
                }

                if(jsonObject.has("descripcion") && !jsonObject.isNull("descripcion")) {
                    item.setDescription(jsonObject.getString("descripcion"));
                }

                if(jsonObject.has("tipo_torneo_string") && !jsonObject.isNull("tipo_torneo_string")) {
                    item.setTournamentTypes(jsonObject.getString("tipo_torneo_string"));
                }

                if (jsonObject.has("distancia") && !jsonObject.isNull("distancia")) {
                    item.setDistance(jsonObject.getDouble("distancia"));
                }

                if(jsonObject.has("pais") && !jsonObject.isNull("pais")) {
                    JSONObject jsonCountryObj = jsonObject.getJSONObject("pais");
                    Country country = new Country();
                    country.setId(jsonCountryObj.getInt("id"));
                    country.setTitle(jsonCountryObj.getString("title"));
                    item.setCountry(country);
                }

                if(jsonObject.has("provincia") && !jsonObject.isNull("provincia")) {
                    JSONObject jsonStateObj = jsonObject.getJSONObject("provincia");
                    State state = new State();
                    state.setId(jsonStateObj.getInt("id"));
                    state.setTitle(jsonStateObj.getString("title"));
                    item.setState(state);
                }

                if(jsonObject.has("localidad") && !jsonObject.isNull("localidad")) {
                    JSONObject jsonCityObj = jsonObject.getJSONObject("localidad");
                    City city = new City();
                    city.setId(jsonCityObj.getInt("id"));
                    city.setTitle(jsonCityObj.getString("title"));
                    item.setCity(city);
                }

                if(jsonObject.has("barrio") && !jsonObject.isNull("barrio")) {
                    JSONObject jsonNeighborhoodObj = jsonObject.getJSONObject("barrio");
                    Neighborhood neighborhood = new Neighborhood();
                    neighborhood.setId(jsonNeighborhoodObj.getInt("id"));
                    neighborhood.setTitle(jsonNeighborhoodObj.getString("title"));
                    item.setNeighborhood(neighborhood);
                }


                if(jsonObject.has("caracteristicas") && !jsonObject.isNull("caracteristicas")) {
                    JSONArray features = jsonObject.getJSONArray("caracteristicas");
                    if (features.length() > 0) {
                        for (int j = 0; j < features.length(); j++) {
                            JSONObject jsonFeatureObj = ((JSONObject) features.get(j));

                            Feature feature = new Feature();
                            feature.setId(jsonFeatureObj.getInt("id"));
                            feature.setTitle(jsonFeatureObj.getString("title"));
                            feature.setCreatedAt(jsonFeatureObj.getString("created_at"));
                            feature.setUpdatedAt(jsonFeatureObj.getString("updated_at"));
                            item.addFeature(feature);
                        }
                    }
                }

                if(jsonObject.has("sedes") && !jsonObject.isNull("sedes")) {
                    JSONArray hosts = jsonObject.getJSONArray("sedes");
                    if (hosts.length() > 0) {
                        for (int j = 0; j < hosts.length(); j++) {
                            JSONObject jsonHostObj = ((JSONObject) hosts.get(j));

                            Headquarter headquarter = new Headquarter();
                            headquarter.setId(jsonHostObj.getInt("id"));
                            headquarter.setTitle(jsonHostObj.getString("title"));
                            headquarter.setCreatedAt(jsonHostObj.getString("created_at"));
                            headquarter.setUpdatedAt(jsonHostObj.getString("updated_at"));
                            headquarter.setDescription(jsonHostObj.getString("descripcion"));
                            headquarter.setNeighborhood(jsonHostObj.getString("barrio_complejo"));

                            if (jsonHostObj.has("complejo") && !jsonHostObj.isNull("complejo")) {
                                JSONObject jsonPlayfield = jsonHostObj.getJSONObject("complejo");
                                if (jsonPlayfield != null) {

                                    headquarter.setPlayfield(PlayfieldService.getPlayfieldObject(jsonPlayfield, this));
                                }
                            }

                            item.addHost(headquarter);
                        }
                    }
                }

                championships.add(item);
            }

            return championships;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Torneos...");
    }
}
