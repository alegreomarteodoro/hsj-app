package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Player;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class PlayerService extends Service{
    public static final String SERVICE_URL = DEFAULT_URL + "/players";

    public PlayerService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> players = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonCityObj = (JSONObject) items.get(i);

                Player item = new Player();
                item.setId(jsonCityObj.getInt("id"));
                item.setTitle(jsonCityObj.getString("title"));

                players.add(item);
            }

            return players;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando...");
    }
}
