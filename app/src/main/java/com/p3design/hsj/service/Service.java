package com.p3design.hsj.service;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.p3design.hsj.app.App;
import com.p3design.hsj.custom.CustomRequest;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 14/7/15.
 */
public abstract class Service {
    public static final String TAG = Service.class.getSimpleName();
    public static final String DEFAULT_URL = App.API_URL;

    public static final String CREATED = "created";
    public static final String LOADING = "loading";
    public static final String COMPLETED = "completed";
    public static final String FAILED = "failed";

    public static int SOCKET_TIMEOUT_MS = 6000;

    protected String serviceUrl;
    protected String latestUrlAccessed;

    protected Context context;
    ServiceObserver observer;
    protected ProgressDialog dialod;
    protected boolean showProgressDialog = false;
    protected String id;

    public Service(Context context, ServiceObserver observer) {
        this.context = context;
        this.observer = observer;
        this.setServiceUrl(DEFAULT_URL);
        this.observer.onServiceStatusChanged(CREATED, this);
    }

    public Service(String id, Context context, ServiceObserver observer) {
        this(context, observer);
        this.id = id;
    }

    public Service(String id, Context context, ServiceObserver observer, boolean showProgressDialog) {
        this(id, context, observer);
        this.showProgressDialog = showProgressDialog;
    }

    public void get() {
        get("");
    }

    public void get(String params) {
        String URL = this.getServiceUrl() + params;
        latestUrlAccessed = URL;

        Cache cache = App.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(URL);
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    List<Entity> entities = parseJsonObject(jsonObject);
                    if(this.observer != null) {
                        this.observer.onGetResponse(getStatus(jsonObject), getMessage(jsonObject), entities, this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if(this.observer != null) {
                        this.observer.onErrorOcurred(e);
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                if(this.observer != null) {
                    this.observer.onErrorOcurred(e);
                }
            }

        } else {
            this.observer.onServiceStatusChanged(LOADING, this);
            if(this.showProgressDialog) {
                openLoadingDialog();
            }
            // making fresh volley get and getting json
            com.android.volley.toolbox.JsonObjectRequest jsonReq = new com.android.volley.toolbox.JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    VolleyLog.d(TAG, "Response: " + response.toString());
                    if (response != null) {
                        List<Entity> entities = parseJsonObject(response);
                        if(Service.this.observer != null) {
                            Service.this.observer.onGetResponse(getStatus(response), getMessage(response), entities, Service.this);
                            Service.this.observer.onServiceStatusChanged(COMPLETED, Service.this);
                        }
                        closeDialog();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    closeDialog();
                    if(Service.this.observer != null) {
                        Service.this.observer.onErrorOcurred(error);
                        Service.this.observer.onServiceStatusChanged(FAILED, Service.this);
                    }
                }
            });

            jsonReq.setRetryPolicy(new DefaultRetryPolicy(
                    SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            // Adding get to volley get queue
            App.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public void post(Map<String, String> params) {

        String URL = this.getServiceUrl();
        latestUrlAccessed = URL;

        this.observer.onServiceStatusChanged(LOADING, this);
        if(this.showProgressDialog) {
            openLoadingDialog();
        }

        Cache cache = App.getInstance().getRequestQueue().getCache();
        cache.remove(URL);

        CustomRequest jsonReq = new CustomRequest(URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                VolleyLog.d(TAG, "Response: " + response.toString());
                if (response != null) {
                    List<Entity> entities = parseJsonObject(response);
                    if(Service.this.observer != null) {
                        Service.this.observer.onPostResponse(getStatus(response), getMessage(response), entities, Service.this);
                        Service.this.observer.onServiceStatusChanged(COMPLETED, Service.this);
                    }
                    closeDialog();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                closeDialog();
                if(Service.this.observer != null) {
                    Service.this.observer.onErrorOcurred(error);
                    Service.this.observer.onServiceStatusChanged(FAILED, Service.this);
                }
            }
        });

        jsonReq.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding get to volley get queue
        App.getInstance().addToRequestQueue(jsonReq);
    }

    public abstract List<Entity> parseJsonObject(JSONObject response);

    private String getStatus(JSONObject response) {
        String status = "";

        try {

            if(response.has("status") && !response.isNull("status")) {
                status = response.getString("status");
            }

        } catch (JSONException e) {

            e.printStackTrace();
            this.observer.onErrorOcurred(e);

        }

        return status;
    }

    private String getMessage(JSONObject response) {
        String message = "";

        try {

            if(response.has("message") && !response.isNull("message")) {
                message = response.getString("message");
            }

        } catch (JSONException e) {

            e.printStackTrace();
            this.observer.onErrorOcurred(e);

        }

        return message;
    }

    public void closeDialog() {
        if (dialod != null) {
            dialod.dismiss();
            dialod = null;
        }
    }

    public void openLoadingDialog(String customMessage) {
        dialod = new ProgressDialog(this.context);
        dialod.setMessage(customMessage);
        dialod.show();
    }

    public String getLatestUrlAccessed() {
        return latestUrlAccessed;
    }

    public void clearServiceCache(boolean clearLatestUrlAccessed) {
        Cache cache = App.getInstance().getRequestQueue().getCache();
        if(clearLatestUrlAccessed) {
            if(latestUrlAccessed != null) cache.remove(latestUrlAccessed);
        } else {
            cache.clear();
        }
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando...");
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public void enabledProgressDialog() {
        this.showProgressDialog = true;
    }

    public void disabledProgressDialog() {
        this.showProgressDialog = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
