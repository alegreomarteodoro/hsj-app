package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.SoilType;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 14/7/15.
 */
public class SoilTypeService extends Service {
    public static final String SERVICE_URL = DEFAULT_URL + "/soil-types";

    public SoilTypeService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> soilTypes = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonStateObj = (JSONObject) items.get(i);

                SoilType item = new SoilType();
                item.setId(jsonStateObj.getInt("id"));
                item.setTitle(jsonStateObj.getString("title"));

                soilTypes.add(item);
            }

            return soilTypes;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Suelos...");
    }
}
