package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Neighborhood;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class NeighborhoodService extends Service{

    public static final String SERVICE_URL = DEFAULT_URL + "/neighborhoods";

    public NeighborhoodService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> neighborhoods = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonObject = (JSONObject) items.get(i);

                Neighborhood item = new Neighborhood();
                item.setId(jsonObject.getInt("id"));
                item.setTitle(jsonObject.getString("title"));
                item.setCityId(jsonObject.getInt("localidad"));

                neighborhoods.add(item);
            }

            return neighborhoods;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Barrios...");
    }
}
