package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.City;
import com.p3design.hsj.entity.Country;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Feature;
import com.p3design.hsj.entity.Field;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Neighborhood;
import com.p3design.hsj.entity.Player;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.entity.SoilType;
import com.p3design.hsj.entity.State;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 14/7/15.
 */
public class PlayfieldService extends Service {

    public static final String SERVICE_URL = DEFAULT_URL + "/playfields";

    public PlayfieldService(String id, Context context, ServiceObserver observer) {
        super(id, context, observer);
        setServiceUrl(SERVICE_URL);
    }

    @Override
    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray playfieldArray = response.getJSONArray("items");

            List<Entity> playfields = new ArrayList<Entity>();

            for (int i = 0; i < playfieldArray.length(); i++) {
                JSONObject jsonPlayfieldObj = (JSONObject) playfieldArray.get(i);

                playfields.add(getPlayfieldObject(jsonPlayfieldObj, this));
            }

            return playfields;
        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Complejos...");
    }

    public static Playfield getPlayfieldObject(JSONObject jsonPlayfieldObj, Service currentService) {

        try {

            Playfield item = new Playfield();
            item.setId(jsonPlayfieldObj.getInt("id"));
            item.setTitle(jsonPlayfieldObj.getString("title"));

            if(jsonPlayfieldObj.has("detail_url") && !jsonPlayfieldObj.isNull("detail_url")) {
                item.setDetailUrl(jsonPlayfieldObj.getString("detail_url"));
            }

            if(jsonPlayfieldObj.has("email") && !jsonPlayfieldObj.isNull("email")) {
                item.setMail(jsonPlayfieldObj.getString("email"));
            }

            if(jsonPlayfieldObj.has("nombre") && !jsonPlayfieldObj.isNull("nombre")) {
                item.setName(jsonPlayfieldObj.getString("nombre"));
            }

            if(jsonPlayfieldObj.has("apellido") && !jsonPlayfieldObj.isNull("apellido")) {
                item.setSurname(jsonPlayfieldObj.getString("apellido"));
            }

            if(jsonPlayfieldObj.has("distancia") && !jsonPlayfieldObj.isNull("distancia")) {
                item.setDistance(jsonPlayfieldObj.getDouble("distancia"));
            }

            if(jsonPlayfieldObj.has("latitud") && !jsonPlayfieldObj.isNull("latitud") && !jsonPlayfieldObj.getString("latitud").equals("null") && !jsonPlayfieldObj.getString("latitud").isEmpty()) {
                item.setLatitude(jsonPlayfieldObj.getDouble("latitud"));
            }

            if(jsonPlayfieldObj.has("longitud") && !jsonPlayfieldObj.isNull("longitud") && !jsonPlayfieldObj.getString("longitud").equals("null") && !jsonPlayfieldObj.getString("longitud").isEmpty()) {
                item.setLongitude(jsonPlayfieldObj.getDouble("longitud"));
            }

            if(jsonPlayfieldObj.has("imagen_destacado") && !jsonPlayfieldObj.isNull("imagen_destacado")) {
                Image featuredImage = new Image();
                featuredImage.setUrl(jsonPlayfieldObj.getString("imagen_destacado"));
                featuredImage.setTitle(item.getTitle());
                item.setFeaturedImage(featuredImage);
            }

            if(jsonPlayfieldObj.has("votos_positivos") && !jsonPlayfieldObj.isNull("votos_positivos")) {
                item.setPositiveVotes(jsonPlayfieldObj.getInt("votos_positivos"));
            }
            item.setAddress(jsonPlayfieldObj.getString("direccion"));
            item.setPhone(jsonPlayfieldObj.getString("telefono"));

            if(!jsonPlayfieldObj.getString("como_llegar").equals("null") ) {
                item.setHowToGet(jsonPlayfieldObj.getString("como_llegar"));
            }

            if(jsonPlayfieldObj.has("caracteristicas") && !jsonPlayfieldObj.isNull("caracteristicas")) {
                JSONArray features = jsonPlayfieldObj.getJSONArray("caracteristicas");
                if (features.length() > 0) {
                    for (int j = 0; j < features.length(); j++) {
                        JSONObject jsonFeatureObj = ((JSONObject) features.get(j));

                        Feature feature = new Feature();
                        feature.setId(jsonFeatureObj.getInt("id"));
                        feature.setTitle(jsonFeatureObj.getString("title"));
                        feature.setCreatedAt(jsonFeatureObj.getString("created_at"));
                        feature.setUpdatedAt(jsonFeatureObj.getString("updated_at"));
                        item.addFeature(feature);
                    }
                }
            }

            if(jsonPlayfieldObj.has("canchas") && !jsonPlayfieldObj.isNull("canchas")) {
                JSONArray fields = jsonPlayfieldObj.getJSONArray("canchas");
                if(fields.length() > 0) {
                    for (int j = 0; j < fields.length(); j++) {
                        JSONObject jsonFieldObj = ((JSONObject) fields.get(j));

                        Field field = new Field();
                        field.setId(jsonFieldObj.getInt("id"));
                        field.setTitle(jsonFieldObj.getString("title"));
                        field.setCreatedAt(jsonFieldObj.getString("created_at"));
                        field.setUpdatedAt(jsonFieldObj.getString("updated_at"));

                        if(jsonFieldObj.has("precio") && !jsonFieldObj.isNull("precio")) {
                            field.setPrice(jsonFieldObj.getDouble("precio"));
                        }

                        if(jsonFieldObj.has("techo") && !jsonFieldObj.isNull("techo")) {
                            field.setCeiling(jsonFieldObj.getBoolean("techo"));
                        }

                        if(jsonFieldObj.has("luz") && !jsonFieldObj.isNull("luz")) {
                            field.setLight(jsonFieldObj.getBoolean("luz"));
                        }

                        if(jsonFieldObj.has("jugadores") && !jsonFieldObj.isNull("jugadores")) {
                            JSONObject jsonJugadores = jsonFieldObj.getJSONObject("jugadores");

                            Player player = new Player();
                            player.setId(jsonJugadores.getInt("id"));
                            player.setTitle(jsonJugadores.getString("title"));
                            player.setCreatedAt(jsonJugadores.getString("created_at"));
                            player.setUpdatedAt(jsonJugadores.getString("updated_at"));

                            field.setPlayer(player);
                        }

                        if(jsonFieldObj.has("suelo") && !jsonFieldObj.isNull("suelo")) {
                            JSONObject jsonSoilType = jsonFieldObj.getJSONObject("suelo");

                            SoilType soilType = new SoilType();
                            soilType.setId(jsonSoilType.getInt("id"));
                            soilType.setTitle(jsonSoilType.getString("title"));
                            soilType.setCreatedAt(jsonSoilType.getString("created_at"));
                            soilType.setUpdatedAt(jsonSoilType.getString("updated_at"));

                            field.setSoilType(soilType);
                        }


                        item.getFields().add(field);
                    }
                }
            }

            JSONObject jsonCountryObj = jsonPlayfieldObj.getJSONObject("pais");
            Country country = new Country();
            country.setId(jsonCountryObj.getInt("id"));
            country.setTitle(jsonCountryObj.getString("title"));
            item.setCountry(country);

            JSONObject jsonStateObj = jsonPlayfieldObj.getJSONObject("provincia");
            State state = new State();
            state.setId(jsonStateObj.getInt("id"));
            state.setTitle(jsonStateObj.getString("title"));
            item.setState(state);

            JSONObject jsonCityObj = jsonPlayfieldObj.getJSONObject("localidad");
            City city = new City();
            city.setId(jsonCityObj.getInt("id"));
            city.setTitle(jsonCityObj.getString("title"));
            item.setCity(city);

            JSONObject jsonNeighborhoodObj = jsonPlayfieldObj.getJSONObject("barrio");
            Neighborhood neighborhood = new Neighborhood();
            neighborhood.setId(jsonNeighborhoodObj.getInt("id"));
            neighborhood.setTitle(jsonNeighborhoodObj.getString("title"));
            item.setNeighborhood(neighborhood);

            if(jsonPlayfieldObj.has("gallery") && !jsonPlayfieldObj.isNull("gallery")) {
                JSONArray gallery = jsonPlayfieldObj.getJSONArray("gallery");
                if (gallery.length() > 0) {
                    for (int j = 0; j < gallery.length(); j++) {
                        JSONObject jsonGalleryObj = ((JSONObject) gallery.get(j));

                        Image image = new Image();
                        image.setUrl(jsonGalleryObj.getString("image_full_url"));
                        image.setTitle(item.getTitle());
                        image.setSubtitle(item.getFeatured());

                        item.getGallery().getImages().add(image);
                    }
                }
            }

            return item;

        } catch (JSONException e) {
            e.printStackTrace();
            currentService.observer.onErrorOcurred(e);
            currentService.observer.onServiceStatusChanged(FAILED, currentService);
        }

        return null;
    }
}
