package com.p3design.hsj.service;

import android.content.Context;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.State;
import com.p3design.hsj.observer.ServiceObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class StateService extends Service{

    public static final String SERVICE_URL = DEFAULT_URL + "/states";

    public StateService(Context context, ServiceObserver observer) {
        super(context, observer);
        setServiceUrl(SERVICE_URL);
    }

    public List<Entity> parseJsonObject(JSONObject response) {
        try {
            JSONArray items = response.getJSONArray("items");

            List<Entity> states = new ArrayList<Entity>();

            for (int i = 0; i < items.length(); i++) {
                JSONObject jsonStateObj = (JSONObject) items.get(i);

                State item = new State();
                item.setId(jsonStateObj.getInt("id"));
                item.setTitle(jsonStateObj.getString("title"));
                item.setCountry(jsonStateObj.getInt("pais"));

                states.add(item);
            }

            return states;

        } catch (JSONException e) {
            e.printStackTrace();
            this.observer.onErrorOcurred(e);
            this.observer.onServiceStatusChanged(FAILED, this);
        }

        return null;
    }

    public void openLoadingDialog() {
        openLoadingDialog("Cargando Provincias...");
    }
}
