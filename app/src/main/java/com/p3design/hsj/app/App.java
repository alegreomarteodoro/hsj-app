package com.p3design.hsj.app;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.p3design.hsj.R;
import com.p3design.hsj.cache.LruBitmapCache;
import com.p3design.hsj.net.Connectivity;
import com.p3design.hsj.util.DynamicView;

/**
 * Created by omarteodoroalegre on 4/7/15.
 */
public class App extends Application {

    public static final String TAG = App.class.getSimpleName();
    public static final String SITE_URL = "http://www.hoysejuega.com";
    public static final String API_URL = SITE_URL + "/api/v1";

    public static final String YOUTUBE_DEVELOPER_KEY = "AIzaSyDELH7GPKvXiBxUcPJGtGDm7knBkQtcprE";

    private static int DISK_IMAGECACHE_SIZE = 1024*1024*100;

    public static final String ERROR_CONNECTION_LAYOUT = "ERROR_CONNECTION_LAYOUT";
    public static final String ERROR_CONNECTION_IN_LIST_LAYOUT = "ERROR_CONNECTION_IN_LIST_LAYOUT";
    public static final String ERROR_LOCATION_LAYOUT = "ERROR_LOCATION_LAYOUT";
    public static final String MAIN_LAYOUT = "MAIN_LAYOUT";
    public static final String LOADING_LAYOUT = "LOADING_LAYOUT";
    public static final String NOT_MATCH_RESULTS_LAYOUT = "NOT_MATCH_RESULTS_LAYOUT";

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private com.nostra13.universalimageloader.core.ImageLoader mUniversalImageLoader;
    private LruBitmapCache mLruBitmapCache;
    private Connectivity connectivityInfo;

    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {

            if(mLruBitmapCache == null) mLruBitmapCache = new LruBitmapCache();

            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
            //mImageLoader = new ImageLoader(this.mRequestQueue, new DiskLruBitmapCache(getApplicationContext(), this.getPackageCodePath(), DISK_IMAGECACHE_SIZE, DISK_IMAGECACHE_COMPRESS_FORMAT, DISK_IMAGECACHE_QUALITY));
        }
        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        return this.mLruBitmapCache;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public Connectivity getConnectivityInfo() {
        if(connectivityInfo == null) {
            connectivityInfo = new Connectivity(getApplicationContext());
        }
        return connectivityInfo;
    }

    public static DynamicView newFragmentDynamicView(Context context) {
        DynamicView dynamicView = new DynamicView(context, DynamicView.ViewType.FRAGMENT, DynamicView.WindowLayoutType.RELATIVE_LAYOUT);
        dynamicView.addViewByLayoutId(LOADING_LAYOUT, R.layout.global_floating_loader);
        dynamicView.addViewByLayoutId(ERROR_CONNECTION_LAYOUT, R.layout.global_connection_unavailable);
        dynamicView.addViewByLayoutId(ERROR_LOCATION_LAYOUT, R.layout.global_location_unavailable);
        return dynamicView;
    }

    public static DynamicView newActivityDynamicView(Context context, int layoutId) {
        DynamicView dynamicView = new DynamicView(context, layoutId, DynamicView.WindowLayoutType.RELATIVE_LAYOUT);
        dynamicView.addViewByLayoutId(App.ERROR_CONNECTION_LAYOUT, R.layout.global_connection_unavailable);
        dynamicView.addViewByLayoutId(App.ERROR_CONNECTION_IN_LIST_LAYOUT, R.layout.global_connection_unavailable_in_list);
        dynamicView.addViewByLayoutId(App.LOADING_LAYOUT, R.layout.global_floating_loader);
        dynamicView.addViewByLayoutId(App.NOT_MATCH_RESULTS_LAYOUT, R.layout.global_not_match_results);
        return dynamicView;
    }

    public com.nostra13.universalimageloader.core.ImageLoader getUniversalImageLoader() {

        if(mUniversalImageLoader == null) {
            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(false)
                    .cacheOnDisk(true)
                    .showImageOnLoading(R.drawable.default_img)
                    .showImageOnFail(R.drawable.default_img)
                    .showImageForEmptyUri(R.drawable.default_img)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .displayer(new FadeInBitmapDisplayer(400))
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                    .defaultDisplayImageOptions(defaultOptions)
                    .writeDebugLogs()
                    .denyCacheImageMultipleSizesInMemory()
                    .memoryCacheSize((1024 * 1024 * 30))
                    .diskCacheSize((1024 * 1024 * 100))
                    .threadPoolSize(5)
                    .diskCacheExtraOptions(480, 320, null)
                    .build();

            mUniversalImageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
            mUniversalImageLoader.init(config);
        }

        return mUniversalImageLoader;
    }


    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                            .setDescription(
                                    new StandardExceptionParser(this, null)
                                            .getDescription(Thread.currentThread().getName(), e))
                            .setFatal(false)
                            .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }
}
