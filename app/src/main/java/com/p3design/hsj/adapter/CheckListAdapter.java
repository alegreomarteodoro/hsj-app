package com.p3design.hsj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.p3design.hsj.R;
import com.p3design.hsj.entity.CheckableItem;

import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class CheckListAdapter extends ArrayAdapter<CheckableItem> {

    private LayoutInflater mLayoutInflater;
    private ArrayList<CheckableItem> mItems;

    public CheckListAdapter(Context context, ArrayList<CheckableItem> objects) {
        super(context, 0);
        mLayoutInflater = LayoutInflater.from(context);
        mItems = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.item_checkable, null);
        }

        final CheckableItem item = (CheckableItem) getItem(position);

        if(item.getCheckBox() == null) {
            CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            item.setCheckBox(checkBox);
        }

        if(item.getTitle() != null) {
            if(CheckableItem.ItemType.PLAYER == item.getItemType()) {
                item.getCheckBox().setText(item.getTitle() + " " + this.getContext().getString(R.string.players));
            } else {
                item.getCheckBox().setText(item.getTitle());
            }
        }

        item.getCheckBox().setChecked(item.isChecked());

        item.getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                item.setChecked(isChecked);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CheckableItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<CheckableItem> getItems() {
        return mItems;
    }

    public void checkedAllItems() {
        for (int i = 0; i < mItems.size(); i++) {
            CheckableItem item = mItems.get(i);
            item.setChecked(!item.isChecked());
            if(item.getCheckBox() != null) item.getCheckBox().setChecked(item.isChecked());
        }
    }
}
