package com.p3design.hsj.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.p3design.hsj.R;
import com.p3design.hsj.activity.DrawerActivity;
import com.p3design.hsj.fragment.FeaturedPlayfieldListFragment;
import com.p3design.hsj.fragment.NearbyPlayfieldsListFragment;
import com.p3design.hsj.fragment.TopRatedPlayfieldListFragment;

/**
 * Created by omarteodoroalegre on 21/1/16.
 */
public class PlayfieldPagerAdapter extends FragmentPagerAdapter {
    DrawerActivity mDrawerActivity;

    public PlayfieldPagerAdapter(DrawerActivity drawerActivity) {
        super(drawerActivity.getSupportFragmentManager());
        mDrawerActivity = drawerActivity;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();

        switch (position) {
            case 0:
                fragment = FeaturedPlayfieldListFragment.newInstance(position);
                break;

            case 1:
                fragment = TopRatedPlayfieldListFragment.newInstance(position);
                break;

            case 2:
                fragment = NearbyPlayfieldsListFragment.newInstance(position);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 0:
                title = mDrawerActivity.getString(R.string.featured_title);
                break;
            case 1:
                title = mDrawerActivity.getString(R.string.top_rated_title);
                break;
            case 2:
                title = mDrawerActivity.getString(R.string.nearest_title);
                break;
        }
        return title;
    }
}
