package com.p3design.hsj.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.p3design.hsj.R;
import com.p3design.hsj.activity.TournamentDetailActivity;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Tournament;
import com.p3design.hsj.util.AnimatorUtils;
import com.p3design.hsj.widget.AnimatedNetworkImageView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class TopRatedTournamentListAdapter extends RecyclerView.Adapter<TopRatedTournamentListAdapter.TopRatedTournamentViewHolder> {

    public static final String TAG = TopRatedTournamentListAdapter.class.getSimpleName();

    protected Activity mActivity;
    protected ArrayList<Entity> mDataset;
    protected int previusPosition = 0;

    public static class TopRatedTournamentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTitle;
        public TextView mPlayers;
        public ImageView mImage;
        public TextView mVotes;
        public TextView mAddress;

        public TopRatedTournamentViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.item_title);
            mPlayers = (TextView) view.findViewById(R.id.item_players);
            mImage = (ImageView) view.findViewById(R.id.item_image);
            mVotes = (TextView) view.findViewById(R.id.item_votes);
            mAddress = (TextView) view.findViewById(R.id.item_address);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public TopRatedTournamentListAdapter(ArrayList<Entity> dataset) {
        this(dataset, null);
    }

    public TopRatedTournamentListAdapter(ArrayList<Entity> dataset, Activity activity) {
        this.mDataset = dataset;
        this.mActivity = activity;
    }

    @Override
    public TopRatedTournamentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_top_rated, parent, false);

        TopRatedTournamentViewHolder vh = new TopRatedTournamentViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final TopRatedTournamentViewHolder holder, int position) {

        final Tournament tournament = (Tournament) mDataset.get(position);

        if(tournament.getFeaturedImage() != null && !tournament.getFeaturedImage().getUrl().equals(App.SITE_URL)) {
            App.getInstance().getUniversalImageLoader().displayImage(tournament.getFeaturedImage().getUrl(), holder.mImage);
        } else if(tournament.getImage() != null) {
            Image imageGallery = tournament.getImage();
            App.getInstance().getUniversalImageLoader().displayImage(imageGallery.getUrl(), holder.mImage);
        } else {
            App.getInstance().getUniversalImageLoader().displayImage("drawable://" +R.drawable.default_img, holder.mImage);
        }

        if(tournament.getTitle() != null) holder.mTitle.setText(tournament.getTitle());

        if(tournament.getTournamentTypes() != null && mActivity != null) {
            String description = mActivity.getString(R.string.soccer) + " " + tournament.getTournamentTypes();
            holder.mPlayers.setText(description);
        }

        if(tournament.getPositiveVotes() != null) holder.mVotes.setText(tournament.getPositiveVotes().toString());

        if(previusPosition<position) {
            AnimatorUtils.translate(holder.itemView, AnimatorUtils.Direction.UP);
        } else {
            AnimatorUtils.translate(holder.itemView, AnimatorUtils.Direction.DOWN);
        }

        previusPosition = position;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, TournamentDetailActivity.class);
                intent.putExtra("tournament", (Serializable) tournament);

                String transitionArticleImage = v.getResources().getString(R.string.transition_id_article_detail_image);
                String transitionArticleTitle = v.getResources().getString(R.string.transition_id_article_detail_title);

                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                                Pair.create((View) holder.mImage, transitionArticleImage),
                                Pair.create((View) holder.mTitle, transitionArticleTitle)
                        );
                ActivityCompat.startActivity(mActivity, intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
