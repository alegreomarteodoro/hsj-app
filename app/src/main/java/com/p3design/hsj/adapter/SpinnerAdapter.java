package com.p3design.hsj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.p3design.hsj.R;
import com.p3design.hsj.entity.Entity;

import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 13/7/15.
 */
public class SpinnerAdapter extends BaseAdapter {

    Context context;
    ArrayList<? extends Entity> entities;

    public SpinnerAdapter(Context context, ArrayList<? extends Entity> entities) {
        this.context = context;
        this.entities = entities;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_spinner, null);
        }

        Entity entity = (Entity) getItem(position);
        CheckedTextView title = (CheckedTextView) convertView;

        if(entity.getTitle() != null) {
            title.setText(entity.getTitle());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return entities.size();
    }

    @Override
    public Object getItem(int position) {
        return entities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}