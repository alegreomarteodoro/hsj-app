package com.p3design.hsj.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.p3design.hsj.R;
import com.p3design.hsj.activity.ContactFormActivity;
import com.p3design.hsj.activity.MapActivity;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.ContactInfoItem;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Playfield;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 12/2/16.
 */
public class ContactInfoAdapter extends RecyclerView.Adapter<ContactInfoAdapter.ContactInfoViewHolder> {

    public static final String TAG = ContactInfoAdapter.class.getSimpleName();

    Activity mActivity;
    private ArrayList<ContactInfoItem> mDataset;

    public static class ContactInfoViewHolder extends RecyclerView.ViewHolder {
        public TextView mPrimaryTitle;
        public TextView mSecondaryTitle;
        public ImageView mPrimaryIcon;
        public ImageView mSecondaryIcon;
        public View mDivider;

        public ContactInfoViewHolder(View view) {
            super(view);
            mPrimaryTitle = (TextView) view.findViewById(R.id.primary_title);
            mSecondaryTitle = (TextView) view.findViewById(R.id.secondary_title);
            mPrimaryIcon = (ImageView) view.findViewById(R.id.primary_icon);
            mSecondaryIcon = (ImageView) view.findViewById(R.id.secondary_icon);
            mDivider = (View) view.findViewById(R.id.item_divider);
        }
    }

    public ContactInfoAdapter(ArrayList<ContactInfoItem> myDataset) {
        this(myDataset, null);
    }

    public ContactInfoAdapter(ArrayList<ContactInfoItem> myDataset, Activity mainActivity) {
        this.mDataset = myDataset;
        this.mActivity = mainActivity;
    }

    @Override
    public ContactInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact_info, parent, false);
        ContactInfoViewHolder vh = new ContactInfoViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ContactInfoViewHolder holder, int position) {
        final ContactInfoItem item = (ContactInfoItem) mDataset.get(position);

        holder.mPrimaryTitle.setText(item.getPrimaryTitle());
        holder.mSecondaryTitle.setText(item.getSecondaryTitle());
        holder.mPrimaryIcon.setImageDrawable(item.getPrimaryIcon());
        holder.mSecondaryIcon.setImageDrawable(item.getSecondaryIcon());

        if (item.getShowDivider() == true) {
            holder.mDivider.setVisibility(View.VISIBLE);
        } else {
            holder.mDivider.setVisibility(View.GONE);
        }

        if (item.getType() == null) return;

        if (item.getType() == ContactInfoItem.ItemType.PHONE) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                    String phone = item.getPrimaryTitle();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    mActivity.startActivity(intent);


                    String trackLabel = "";
                    String trackCategory = "";

                    if(item.getEntity() != null) {
                        if(item.getEntity() instanceof Playfield) {
                            trackCategory = "Canchas";
                        } else {
                            trackCategory = "Torneos";
                        }

                        trackLabel = item.getEntity().getTitle() + " - " + item.getEntity().getId();
                    }

                    App.getInstance().trackEvent(trackCategory, "Llamada", trackLabel);
                }
            });
        } else if (item.getType() == ContactInfoItem.ItemType.ADDRESS) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, MapActivity.class);
                    intent.putExtra("entity", (Serializable) item.getEntity());

                    mActivity.startActivity(intent);


                    String trackLabel = "";
                    String trackCategory = "";

                    if(item.getEntity() != null) {
                        if(item.getEntity() instanceof Playfield) {
                            trackCategory = "Canchas";
                        } else {
                            trackCategory = "Torneos";
                        }

                        trackLabel = item.getEntity().getTitle() + " - " + item.getEntity().getId();
                    }

                    App.getInstance().trackEvent(trackCategory, "Se visitó el mapa", trackLabel);
                }
            });
        } else if (item.getType() == ContactInfoItem.ItemType.CONTACT_INFO) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, ContactFormActivity.class);
                    intent.putExtra("title", item.getPrimaryTitle());
                    intent.putExtra("description", v.getResources().getString(R.string.contact_form_description2));
                    intent.putExtra("entity", (Serializable) item.getEntity());

                    mActivity.startActivity(intent);



                    String trackLabel = "";
                    String trackCategory = "";

                    if(item.getEntity() != null) {
                        if(item.getEntity() instanceof Playfield) {
                            trackCategory = "Canchas";
                        } else {
                            trackCategory = "Torneos";
                        }

                        trackLabel = item.getEntity().getTitle() + " - " + item.getEntity().getId();
                    }

                    App.getInstance().trackEvent(trackCategory, "Ingreso a Contacto", trackLabel);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
