package com.p3design.hsj.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

import com.p3design.hsj.entity.Image;
import com.p3design.hsj.fragment.SwipeGalleryFragment;

import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 1/9/15.
 */
public class GalleryPagerAdapter extends FragmentPagerAdapter {
    protected ArrayList<Image> gallery = new ArrayList<Image>();
    protected FragmentActivity context;

    public GalleryPagerAdapter(FragmentActivity context, ArrayList<Image> gallery) {
        super(context.getSupportFragmentManager());
        this.context = context;
        this.gallery.addAll(gallery);
    }

    @Override
    public int getCount() {
        return this.gallery.size();
    }

    @Override
    public Fragment getItem(int position) {
        Image currentImage = this.gallery.get(position);
        if(currentImage.getTitle() != null)
            context.setTitle(currentImage.getTitle());
        return SwipeGalleryFragment.newInstance(currentImage);
    }
}