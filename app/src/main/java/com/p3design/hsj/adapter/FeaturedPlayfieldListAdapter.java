package com.p3design.hsj.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.p3design.hsj.R;
import com.p3design.hsj.activity.PlayfieldDetailActivity;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.util.AnimatorUtils;
import com.p3design.hsj.widget.AnimatedNetworkImageView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 21/1/16.
 */
public class FeaturedPlayfieldListAdapter extends RecyclerView.Adapter<FeaturedPlayfieldListAdapter.FeaturedPlayfieldViewHolder> {

    public static final String TAG = FeaturedPlayfieldListAdapter.class.getSimpleName();

    protected ImageLoader imageLoader = App.getInstance().getImageLoader();
    protected Activity mActivity;
    protected ArrayList<Entity> mDataset;

    public static class FeaturedPlayfieldViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mCount;
        public ImageView mImage;
        public TextView mVotes;

        public FeaturedPlayfieldViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.item_title);
            mCount = (TextView) view.findViewById(R.id.item_description);
            mImage = (ImageView) view.findViewById(R.id.item_image);
            mVotes = (TextView) view.findViewById(R.id.item_votes);
        }
    }

    public FeaturedPlayfieldListAdapter(ArrayList<Entity> dataset) {
        this(dataset, null);
    }

    public FeaturedPlayfieldListAdapter(ArrayList<Entity> dataset, Activity activity) {
        this.mDataset = dataset;
        this.mActivity = activity;
    }

    @Override
    public FeaturedPlayfieldListAdapter.FeaturedPlayfieldViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_featured, parent, false);

        FeaturedPlayfieldViewHolder vh = new FeaturedPlayfieldViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FeaturedPlayfieldViewHolder holder, int position) {

        final Playfield playfield = (Playfield) mDataset.get(position);

        if(playfield.getFeaturedImage() != null && !playfield.getFeaturedImage().getUrl().equals(App.SITE_URL)) {
            App.getInstance().getUniversalImageLoader().displayImage(playfield.getFeaturedImage().getUrl(), holder.mImage);
        } else if(playfield.getGallery().getImages().size() > 0) {
            Image imageGallery = playfield.getGallery().getImages().get(0);
            App.getInstance().getUniversalImageLoader().displayImage(imageGallery.getUrl(), holder.mImage);
        } else {
            App.getInstance().getUniversalImageLoader().displayImage("drawable://" +R.drawable.default_img, holder.mImage);
        }

        if(playfield.getTitle() != null) holder.mTitle.setText(playfield.getTitle());

        if(mActivity != null) {
            int fieldCount = 0;
            if (playfield.getFields().size() > 0) {
                fieldCount = playfield.getFields().size();
            }
            holder.mCount.setText(fieldCount + " " + mActivity.getResources().getString(R.string.playfields));
        }

        if(playfield.getPositiveVotes() != null) holder.mVotes.setText(playfield.getPositiveVotes().toString());


        AnimatorUtils.translate(holder.itemView, AnimatorUtils.Direction.UP);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, PlayfieldDetailActivity.class);
                intent.putExtra("playfield", (Serializable) playfield);

                String transitionArticleImage = v.getResources().getString(R.string.transition_id_article_detail_image);
                String transitionArticleTitle = v.getResources().getString(R.string.transition_id_article_detail_title);

                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                                Pair.create((View) holder.mImage, transitionArticleImage),
                                Pair.create((View) holder.mTitle, transitionArticleTitle)
                        );
                ActivityCompat.startActivity(mActivity, intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
