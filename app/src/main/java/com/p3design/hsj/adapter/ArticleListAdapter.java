package com.p3design.hsj.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.p3design.hsj.R;
import com.p3design.hsj.activity.ArticleDetailActivity;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Article;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.util.AnimatorUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 5/7/15.
 */
public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder> {

    public static final String TAG = ArticleListAdapter.class.getSimpleName();

    Activity activity;
    private ArrayList<Entity> mDataset;
    protected int previusPosition = 0;

    public static class ArticleViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mCategory;
        public ImageView mImage;
        public Button mButtonViewMore;

        public ArticleViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.article_list_item_title);
            mCategory = (TextView) view.findViewById(R.id.article_list_item_category);
            mImage = (ImageView) view.findViewById(R.id.article_list_item_image);
            mButtonViewMore = (Button) view.findViewById(R.id.article_list_item_read_more);
        }
    }

    public ArticleListAdapter(ArrayList<Entity> myDataset) {
        this(myDataset, null);
    }

    public ArticleListAdapter(ArrayList<Entity> myDataset, Activity mainActivity) {
        this.mDataset = myDataset;
        this.activity = mainActivity;
    }

    @Override
    public int getItemViewType(int position) {
        Article article = (Article) mDataset.get(position);
        int type = 0; // estandar article
        if(article.getFeatured()) {
            type = 1; // featured article
        }
        return type;
    }

    @Override
    public ArticleListAdapter.ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_article, parent, false);
        if(viewType == 1) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_featured_article, parent, false);
        }
        ArticleViewHolder vh = new ArticleViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ArticleViewHolder holder, int position) {
        final Article article = (Article) mDataset.get(position);

        if(article.getFeaturedImage() != null) {
            App.getInstance().getUniversalImageLoader().displayImage(article.getFeaturedImage().getUrl(), holder.mImage);
        } else {
            App.getInstance().getUniversalImageLoader().displayImage("drawable://" +R.drawable.default_img, holder.mImage);
        }

        holder.mTitle.setText(article.getTitle());
        holder.mCategory.setText(article.getCategory());

        holder.mButtonViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ArticleDetailActivity.class);
                intent.putExtra("article", (Serializable) article);

                String transitionArticleImage = v.getResources().getString(R.string.transition_id_article_detail_image);
                String transitionArticleTitle = v.getResources().getString(R.string.transition_id_article_detail_title);

                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                                Pair.create((View) holder.mImage, transitionArticleImage),
                                Pair.create((View) holder.mTitle, transitionArticleTitle)
                        );
                ActivityCompat.startActivity(activity, intent, options.toBundle());
            }
        });

        if(previusPosition<position) {
            AnimatorUtils.translate(holder.itemView, AnimatorUtils.Direction.UP);
        } else {
            AnimatorUtils.translate(holder.itemView, AnimatorUtils.Direction.DOWN);
        }

        previusPosition = position;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
