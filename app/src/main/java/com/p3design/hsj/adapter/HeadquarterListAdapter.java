package com.p3design.hsj.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.p3design.hsj.R;
import com.p3design.hsj.activity.PlayfieldDetailActivity;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.util.AnimatorUtils;
import com.p3design.hsj.widget.AnimatedNetworkImageView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 19/2/16.
 */
public class HeadquarterListAdapter  extends RecyclerView.Adapter<HeadquarterListAdapter.HeadquarterViewHolder> {

    public static final String TAG = HeadquarterListAdapter.class.getSimpleName();

    protected Activity mActivity;
    protected ArrayList<Entity> mDataset;

    public static class HeadquarterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mTitle;
        public TextView mPlayers;
        public ImageView mImage;
        public TextView mVotes;
        public TextView mAddress;
        public View mDivider;

        public HeadquarterViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.item_title);
            mPlayers = (TextView) view.findViewById(R.id.item_players);
            mImage = (ImageView) view.findViewById(R.id.item_image);
            mVotes = (TextView) view.findViewById(R.id.item_votes);
            mAddress = (TextView) view.findViewById(R.id.item_address);
            mDivider = (View) view.findViewById(R.id.item_divider);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public HeadquarterListAdapter(ArrayList<Entity> dataset) {
        this(dataset, null);
    }

    public HeadquarterListAdapter(ArrayList<Entity> dataset, Activity activity) {
        this.mDataset = dataset;
        this.mActivity = activity;
    }

    @Override
    public HeadquarterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_top_rated, parent, false);

        HeadquarterViewHolder vh = new HeadquarterViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final HeadquarterViewHolder holder, int position) {

        final Playfield playfield = (Playfield) mDataset.get(position);

        if(playfield.getFeaturedImage() != null && !playfield.getFeaturedImage().getUrl().equals(App.SITE_URL)) {
            App.getInstance().getUniversalImageLoader().displayImage(playfield.getFeaturedImage().getUrl(), holder.mImage);
        } else if(playfield.getGallery().getImages().size() > 0) {
            Image imageGallery = playfield.getGallery().getImages().get(0);
            App.getInstance().getUniversalImageLoader().displayImage(imageGallery.getUrl(), holder.mImage);
        } else {
            App.getInstance().getUniversalImageLoader().displayImage("drawable://" + R.drawable.default_img, holder.mImage);
        }

        if(playfield.getTitle() != null) holder.mTitle.setText(playfield.getTitle());

        if(mActivity != null) {
            int fieldCount = 0;
            if (playfield.getFields().size() > 0) {
                fieldCount = playfield.getFields().size();
            }
            holder.mPlayers.setText(fieldCount + " " + mActivity.getResources().getString(R.string.playfields));
        }

        if(playfield.getAddress() != null) holder.mAddress.setText(playfield.getAddress());

        if(playfield.getPositiveVotes() != null) holder.mVotes.setText(playfield.getPositiveVotes().toString());

        if(getItemCount()-1 == position) holder.mDivider.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, PlayfieldDetailActivity.class);
                intent.putExtra("playfield", (Serializable) playfield);

                String transitionArticleImage = v.getResources().getString(R.string.transition_id_article_detail_image);
                String transitionArticleTitle = v.getResources().getString(R.string.transition_id_article_detail_title);

                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                                Pair.create((View) holder.mImage, transitionArticleImage),
                                Pair.create((View) holder.mTitle, transitionArticleTitle)
                        );
                ActivityCompat.startActivity(mActivity, intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}