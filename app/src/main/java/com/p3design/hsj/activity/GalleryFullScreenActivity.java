package com.p3design.hsj.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.GalleryPagerAdapter;
import com.p3design.hsj.entity.Gallery;

public class GalleryFullScreenActivity extends FragmentActivity {
    ViewPager viewPager;
    GalleryPagerAdapter galleryPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_full_screen);

        Gallery gallery = (Gallery) getIntent().getSerializableExtra("gallery");

        galleryPagerAdapter = new GalleryPagerAdapter(this, gallery.getImages());
        viewPager = (ViewPager) findViewById(R.id.gallery_pager);
        viewPager.setAdapter(galleryPagerAdapter);

        int current = getIntent().getIntExtra("current", 0);
        viewPager.setCurrentItem(current);
    }
}
