package com.p3design.hsj.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.p3design.hsj.R;
import com.p3design.hsj.entity.Headquarter;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.entity.Tournament;
import com.p3design.hsj.util.AnimatorUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 16/2/16.
 */
public class MapActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener {

    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private Playfield currentPlayfield;
    private Tournament currentTournament;
    private Map<Marker, Playfield> mMarkers = new HashMap<Marker, Playfield>();
    private RelativeLayout mInfoWindowLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        if(getIntent().getSerializableExtra("entity") instanceof Playfield) {
            currentPlayfield = (Playfield) getIntent().getSerializableExtra("entity");
        } else if(getIntent().getSerializableExtra("entity") instanceof Tournament) {
            currentTournament = (Tournament) getIntent().getSerializableExtra("entity");
        }

        mMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        if (mMapFragment != null) {
            mMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    loadMap(map);
                }
            });
        }

        //toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        getSupportActionBar().setTitle(getString(R.string.location));


    }

    protected void loadMap(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            //mLastLocation = mLocationTracker.getLastLocation();
            //if (mLastLocation != null) {

            //}

            mMap.setOnMarkerClickListener(this);

            loadMarker();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mMap.setMyLocationEnabled(true);
        }
    }

    protected void centerMapIn(LatLng latLng) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
        mMap.animateCamera(cameraUpdate);
    }

    protected void loadMarker() {
        LatLng latLng;
        if(currentPlayfield != null) {
            latLng = new LatLng(currentPlayfield.getLatitude(), currentPlayfield.getLongitude());

            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_playfield_marker)));

            mMarkers.put(marker, currentPlayfield);

            centerMapIn(latLng);

        } else if(currentTournament != null) {
            if(currentTournament.getHosts().size() > 0) {
                for (int i = 0; i < currentTournament.getHosts().size(); i++) {
                    Headquarter headquarter = currentTournament.getHosts().get(i);
                    if(headquarter.getPlayfield() != null) {
                        latLng = new LatLng(headquarter.getPlayfield().getLatitude(), headquarter.getPlayfield().getLongitude());

                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_tournament_marker)));

                        mMarkers.put(marker, headquarter.getPlayfield());

                        if (i == 0) centerMapIn(latLng);
                    }
                }
            }
        }
    }

    protected String makeMapsDirectionsURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString.append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString.append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mMarkers.containsKey(marker)) {
            loadInfoWindow(mMarkers.get(marker));
            if(mInfoWindowLayout.getVisibility() == View.GONE) {
                mInfoWindowLayout.setVisibility(View.VISIBLE);
                AnimatorUtils.translate(mInfoWindowLayout, AnimatorUtils.Direction.UP);
            }
        }
        return false;
    }

    private void loadInfoWindow(Playfield playfield) {
        if(mInfoWindowLayout == null) mInfoWindowLayout = (RelativeLayout) findViewById(R.id.info_window);

        TextView infoWindowTitle = (TextView) mInfoWindowLayout.findViewById(R.id.info_window_title);
        TextView infoWindowDescription = (TextView) mInfoWindowLayout.findViewById(R.id.info_window_description);
        LinearLayout infoWindowIndication = (LinearLayout) mInfoWindowLayout.findViewById(R.id.info_window_indication);
        TextView infoWindowDistance = (TextView) mInfoWindowLayout.findViewById(R.id.info_window_distance);

        infoWindowTitle.setText(playfield.getTitle());
        infoWindowDescription.setText(playfield.getAddress());
        infoWindowDistance.setText(String.format("%.1f", playfield.getDistance()) + " " + getString(R.string.km));
        if(playfield.getDistance() == 0) {
            infoWindowIndication.setVisibility(View.GONE);
        }
    }

    private void closeInfoWindow() {
        if(mInfoWindowLayout != null) mInfoWindowLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if(mInfoWindowLayout != null && mInfoWindowLayout.getVisibility() == View.VISIBLE) {
            closeInfoWindow();
            String message = getString(R.string.close_map_message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } else super.onBackPressed();
    }
}
