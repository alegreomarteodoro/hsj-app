package com.p3design.hsj.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.CheckListAdapter;
import com.p3design.hsj.entity.CheckableItem;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.SoilType;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.service.SoilTypeService;
import com.p3design.hsj.util.SearchParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class SoilTypesPopupActivity extends PopupActivity implements ServiceObserver {

    private Button mCancelButton;
    private Button mOkButton;

    private ListView mSoilTypesList;
    private ProgressBar mProgressBar;
    private CheckListAdapter mCheckListAdapter;
    private ArrayList<CheckableItem> mItems = new ArrayList<CheckableItem>();
    private SoilTypeService mSoilTypeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soiltype_popup);

        if(mProgressBar == null) mProgressBar = (ProgressBar) findViewById(R.id.loading);

        mSoilTypesList = (ListView) findViewById(R.id.soil_type);
        mCheckListAdapter = new CheckListAdapter(this, mItems);
        mSoilTypesList.setAdapter(mCheckListAdapter);

        mSoilTypeService = new SoilTypeService(this, this);
        mSoilTypeService.disabledProgressDialog();
        mSoilTypeService.get();

        setupCancelButtonListeners();
        setupOkButtonListeners();
        setupCheckedAll();
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            for (int i = 0; i < entities.size(); i++) {
                SoilType soilType = (SoilType) entities.get(i);
                mItems.add(new CheckableItem(soilType.getId(), soilType.getTitle()));
            }
            mCheckListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(status.equals(Service.LOADING)) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void setupCancelButtonListeners() {
        if(mCancelButton == null) mCancelButton = (Button) findViewById(R.id.cancel);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoilTypesPopupActivity.this.finish();
            }
        });
    }

    private void setupOkButtonListeners() {
        if(mOkButton == null) mOkButton = (Button) findViewById(R.id.ok);

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ids = "";
                String values = "";
                ArrayList<CheckableItem> itemsSelected = getItemsSelected();

                for (int i = 0; i < itemsSelected.size(); i++) {
                    CheckableItem checkableItem = itemsSelected.get(i);
                    if (checkableItem.isChecked()) {
                        ids += String.valueOf(checkableItem.getId());
                        if (i != (itemsSelected.size() - 1)) ids += ",";
                        values += String.valueOf(checkableItem.getTitle());
                        if (i != (itemsSelected.size() - 1)) values += ", ";
                    }
                }

                if (!ids.isEmpty() && !values.isEmpty()) {
                    SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.SOIL_TYPES_KEY, ids, values);
                }

                finish();
            }
        });
    }

    private ArrayList<CheckableItem> getItemsSelected() {
        ArrayList<CheckableItem> checkableItems = new ArrayList<CheckableItem>();
        for(int i = 0; i < mItems.size(); i++) {
            CheckableItem item = mItems.get(i);
            if(item.isChecked()) {
                checkableItems.add(item);
            }
        }
        return checkableItems;
    }

    private void setupCheckedAll() {
        final CheckBox checkedAll = (CheckBox) findViewById(R.id.checked_all);
        checkedAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckListAdapter.checkedAllItems();
            }
        });
    }
}