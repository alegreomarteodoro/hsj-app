package com.p3design.hsj.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.p3design.hsj.R;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.SearchParamObserver;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.SearchParams;

import java.util.List;

/**
 * Created by omarteodoroalegre on 22/2/16.
 */
public class SearchFormActivity extends AppCompatActivity implements ServiceObserver, SearchParamObserver{
    private View mLocationCardLayout;
    private View mSoilTypeCardLayout;
    private View mPlayersCountCardLayout;
    private View mInfraestructureCardLayout;
    private EditText mQueryStringEditText;
    private Button mClearButton;
    private Button mSearchButton;

    //Soil Types
    private TextView mSoilTypeTitleTextView;
    private TextView mSoilTypeValueTextView;
    //Location
    private TextView mLocationTitleTextView;
    private TextView mLocationValueTextView;
    //Players Count
    private TextView mPlayersTitleTextView;
    private TextView mPlayersValueTextView;
    //infraestructure
    private TextView mInfraestructureTitleTextView;
    private TextView mInfraestructureValueTextView;

    private int mSearchContext;

    public static class SearchContext {
        public static final int PLAYFIELDS = 1;
        public static final int TOURNAMENT = 2;
    }

    private static SearchParams mSearchParams;
    public static SearchParams getSearchParamsInstance() {
        if(mSearchParams == null) mSearchParams = new SearchParams();
        return mSearchParams;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_form);

        getSearchParamsInstance().setObserver(this);
        getSearchParamsInstance().getFilters().clear();

        mSearchContext = getIntent().getIntExtra("searchContext", SearchContext.PLAYFIELDS);


        mQueryStringEditText = (EditText) findViewById(R.id.query_search);

        setupLocationCardLayout(null);
        setupInfraestructureCardLayout();
        setupPlayersCountCardLayout();
        setupSoilTypeCardLayout();

        setupSearchButtonListeners();
        setupClearButtonListeners();

        //toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle(getString(R.string.location));

        changeStatusBarColor();
    }

    private void checkQueryString() {
        if(mQueryStringEditText == null) mQueryStringEditText = (EditText) findViewById(R.id.query_search);

        if(mQueryStringEditText.getText().toString().length() > 0) {
            getSearchParamsInstance().addParam(SearchParams.QUERY_STRING_KEY, mQueryStringEditText.getText().toString(), mQueryStringEditText.getText().toString());
        } else {
            getSearchParamsInstance().removeParam(SearchParams.QUERY_STRING_KEY);
        }
    }

    private void setupSearchButtonListeners() {
        if(mSearchButton == null) mSearchButton = (Button) findViewById(R.id.search);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkQueryString();

                String currentParamsString = getSearchParamsInstance().toString();

                Intent intent = new Intent(SearchFormActivity.this, SearchResultActivity.class);
                intent.putExtra("searchContext", mSearchContext);
                intent.putExtra("searchParams", currentParamsString);

                startActivity(intent);
            }
        });
    }

    private void setupClearButtonListeners() {
        if(mClearButton == null) mClearButton = (Button) findViewById(R.id.clear);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSearchParamsInstance().removeAllParams();
            }
        });
    }

    private void setupLocationCardLayout(Location location) {
        if(mLocationCardLayout == null) mLocationCardLayout = findViewById(R.id.location_card);

        if(mLocationTitleTextView == null) mLocationTitleTextView = (TextView) mLocationCardLayout.findViewById(R.id.title);
        ImageView icon = (ImageView) mLocationCardLayout.findViewById(R.id.icon);
        if(mLocationValueTextView == null) mLocationValueTextView = (TextView) mLocationCardLayout.findViewById(R.id.value);

        icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_place_black_24px));
        mLocationTitleTextView.setText(Html.fromHtml(getString(R.string.set_my_location)));

        mLocationCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent locationPopupIntent = new Intent(SearchFormActivity.this, LocationPopupActivity.class);
                startActivity(locationPopupIntent);
            }
        });
    }

    private void setupSoilTypeCardLayout() {
        if(mSoilTypeCardLayout == null) mSoilTypeCardLayout = findViewById(R.id.soil_type_card);

        if(mSoilTypeTitleTextView == null) mSoilTypeTitleTextView = (TextView) mSoilTypeCardLayout.findViewById(R.id.title);
        ImageView icon = (ImageView) mSoilTypeCardLayout.findViewById(R.id.icon);
        if(mSoilTypeValueTextView == null) mSoilTypeValueTextView = (TextView) mSoilTypeCardLayout.findViewById(R.id.value);

        icon.setImageDrawable(getResources().getDrawable(R.mipmap.ic_light_of_playfield));

        mSoilTypeTitleTextView.setText(Html.fromHtml(getString(R.string.soil_types)));

        mSoilTypeCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent soilTypePopupIntent = new Intent(SearchFormActivity.this, SoilTypesPopupActivity.class);
                startActivity(soilTypePopupIntent);
            }
        });
    }

    private void setupPlayersCountCardLayout() {
        if(mPlayersCountCardLayout == null) mPlayersCountCardLayout = findViewById(R.id.players_count_card);

        if(mPlayersTitleTextView == null) mPlayersTitleTextView = (TextView) mPlayersCountCardLayout.findViewById(R.id.title);
        ImageView icon = (ImageView) mPlayersCountCardLayout.findViewById(R.id.icon);
        if(mPlayersValueTextView == null) mPlayersValueTextView = (TextView) mPlayersCountCardLayout.findViewById(R.id.value);

        icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_soccer_shoes));

        mPlayersTitleTextView.setText(Html.fromHtml(getString(R.string.players)));

        mPlayersCountCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent playersPopupIntent = new Intent(SearchFormActivity.this, PlayersPopupActivity.class);
                startActivity(playersPopupIntent);
            }
        });
    }

    private void setupInfraestructureCardLayout() {
        if(mInfraestructureCardLayout == null) mInfraestructureCardLayout = findViewById(R.id.infrastructure_card);

        if(mInfraestructureTitleTextView == null) mInfraestructureTitleTextView = (TextView) mInfraestructureCardLayout.findViewById(R.id.title);
        ImageView icon = (ImageView) mInfraestructureCardLayout.findViewById(R.id.icon);
        if(mInfraestructureValueTextView == null) mInfraestructureValueTextView = (TextView) mInfraestructureCardLayout.findViewById(R.id.value);

        icon.setImageDrawable(getResources().getDrawable(R.mipmap.ic_light_of_playfield));

        mInfraestructureTitleTextView.setText(Html.fromHtml(getString(R.string.infraestructure)));

        mInfraestructureCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent infraestructurePopupIntent = new Intent(SearchFormActivity.this, InfraestructurePopupActivity.class);
                startActivity(infraestructurePopupIntent);
            }
        });
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }

    @Override
    public void onParamAdded(String key, Object value, String visibleValue) {
        if(key.equals(SearchParams.PLAYERS_KEY)) {
            mPlayersValueTextView.setText(visibleValue);
        }
        if(key.equals(SearchParams.COUNTRY_KEY) ||
                key.equals(SearchParams.STATE_KEY) ||
                key.equals(SearchParams.CITY_KEY) ||
                key.equals(SearchParams.NEIGHBORHOOD_KEY) ||
                key.equals(SearchParams.LOCATION_KEY)) {
            mLocationValueTextView.setText(visibleValue);
        }
        if(key.equals(SearchParams.INFRAESTRUCTURE_KEY)) {
            mInfraestructureValueTextView.setText(visibleValue);
        }
        if(key.equals(SearchParams.SOIL_TYPES_KEY)) {
            mSoilTypeValueTextView.setText(visibleValue);
        }
    }

    @Override
    public void onParamRemoved(String key) {
        if(key.equals(SearchParams.COUNTRY_KEY) ||
                key.equals(SearchParams.STATE_KEY) ||
                key.equals(SearchParams.CITY_KEY) ||
                key.equals(SearchParams.NEIGHBORHOOD_KEY) ||
                key.equals(SearchParams.LOCATION_KEY)) {
            mLocationValueTextView.setText("");
        }
        if(key.equals(SearchParams.PLAYERS_KEY)) {
            mPlayersValueTextView.setText("");
        }
        if(key.equals(SearchParams.INFRAESTRUCTURE_KEY)) {
            mInfraestructureValueTextView.setText("");
        }
        if(key.equals(SearchParams.SOIL_TYPES_KEY)) {
            mSoilTypeValueTextView.setText("");
        }
        if(key.equals(SearchParams.QUERY_STRING_KEY)) {
            mQueryStringEditText.setText("");
        }
    }

    private void changeStatusBarColor() {
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimary));
        }
    }
}
