package com.p3design.hsj.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.TournamentPagerAdapter;
import com.p3design.hsj.util.AnimatorUtils;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class TournamentListActivity extends DrawerActivity {

    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private TournamentPagerAdapter mTournamentPagerAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_list);

        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCollapsingToolbarLayout.setTitleEnabled(false);

        mTournamentPagerAdapter = new TournamentPagerAdapter(this);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(mTournamentPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);

        mNavigationView.getMenu().getItem(2).setChecked(true);
        mToolbar.setTitle(getString(R.string.menu_tournaments));

        int statusBarHeight = (int) Math.ceil(25 * Resources.getSystem().getDisplayMetrics().density);
        int toolbarHeight = (int) Math.ceil((56*2) * Resources.getSystem().getDisplayMetrics().density);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbarHeight = toolbarHeight + statusBarHeight;
        }

        mToolbar.getLayoutParams().height = toolbarHeight;
        mCollapsingToolbarLayout.getLayoutParams().height = toolbarHeight;

        Log.v("HSJ APP", " toolbarHeight --------- " + toolbarHeight);
        Log.v("HSJ APP", " statusBarHeight --------- " + statusBarHeight);
        Log.v("HSJ APP", " density --------- " + Resources.getSystem().getDisplayMetrics().density);

        FloatingActionButton searchFab = (FloatingActionButton) findViewById(R.id.search_fab);
        searchFab.setColorFilter(Color.argb(255, 255, 255, 255));
        searchFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TournamentListActivity.this, SearchFormActivity.class);
                intent.putExtra("searchContext", SearchFormActivity.SearchContext.TOURNAMENT);
                startActivity(intent);
            }
        });

        AnimatorUtils.translate(searchFab, AnimatorUtils.Direction.RIGHT);
    }
}
