package com.p3design.hsj.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.ContactInfoAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.ContactInfoItem;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Feature;
import com.p3design.hsj.entity.Field;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.PlayfieldService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.AnimatorUtils;
import com.p3design.hsj.util.CustomSocialShare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 11/2/16.
 */
public class PlayfieldDetailActivity extends AppCompatActivity implements ServiceObserver{

    private Playfield currentPlayfield;
    private RecyclerView mmContactInfoList;
    private LinearLayoutManager mLinearLayoutManager;
    private ContactInfoAdapter mContactInfoAdapter;
    private CardView mCardviewPositiveVote;

    private FloatingActionButton mFacebookButton;
    private FloatingActionButton mTwitterButton;

    TextView mPrimaryPositiveVotesCount;
    TextView mSecondaryPositiveVotesCount;

    private PlayfieldService mPlayfieldService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playfield_detail);

        currentPlayfield = (Playfield) getIntent().getSerializableExtra("playfield");

        final ImageView playfieldImageView = (ImageView) findViewById(R.id.playfield_featured_image);
        TextView playfieldTitle = (TextView) findViewById(R.id.playfield_title);

        playfieldTitle.setText(currentPlayfield.getTitle());

        playfieldImageView.setImageResource(R.drawable.default_img);

        if(currentPlayfield.getFeaturedImage() != null && !currentPlayfield.getFeaturedImage().getUrl().equals(App.SITE_URL)) {
            App.getInstance().getUniversalImageLoader().displayImage(currentPlayfield.getFeaturedImage().getUrl(), playfieldImageView);
        } else if(currentPlayfield.getGallery().getImages().size() > 0) {
            Image imageGallery = currentPlayfield.getGallery().getImages().get(0);
            App.getInstance().getUniversalImageLoader().displayImage(imageGallery.getUrl(), playfieldImageView);
        }

        //detail list setup
        mmContactInfoList = (RecyclerView) findViewById(R.id.contact_info_list);
        mContactInfoAdapter = new ContactInfoAdapter(loadContactInfo(), this);
        mmContactInfoList.setAdapter(mContactInfoAdapter);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mmContactInfoList.setLayoutManager(mLinearLayoutManager);
        mmContactInfoList.setNestedScrollingEnabled(false);

        loadInfo();
        loadGallery();
        loadFieldList();
        loadPositiveVotes();

        mCardviewPositiveVote = (CardView) findViewById(R.id.cardview_positive_vote);
        mCardviewPositiveVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPositiveVotes();
            }
        });

        //toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //collapsingtoolbarlayout setup
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(currentPlayfield.getTitle());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.AppTheme_Widget_ExpandedAppBar_Titleless);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.AppTheme_Widget_CollapsedAppBar);

        initShareButtons();
    }

    private ArrayList<com.p3design.hsj.entity.ContactInfoItem> loadContactInfo() {
        ArrayList<ContactInfoItem> detailList = new ArrayList<ContactInfoItem>();

        //phone list
        if (currentPlayfield.getPhone() != null && currentPlayfield.getPhone().contains("/")) {

            String[] phonesList = currentPlayfield.getPhone().split("/");
            int phonesLenght = phonesList.length;
            int i = 1;
            for (String phone: phonesList) {
                ContactInfoItem item = new ContactInfoItem();
                item.setPrimaryTitle(phone.trim());
                item.setSecondaryTitle(getString(R.string.click_to_call));
                item.setPrimaryIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
                item.setType(ContactInfoItem.ItemType.PHONE);

                if (i == phonesLenght) {
                    item.setShowDivider(true);
                } else {
                    item.setShowDivider(false);
                }

                detailList.add(item);

                i++;
            }

        } else {

            ContactInfoItem item = new ContactInfoItem();
            item.setPrimaryTitle(currentPlayfield.getPhone());
            item.setSecondaryTitle(getString(R.string.click_to_call));
            item.setPrimaryIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
            item.setType(ContactInfoItem.ItemType.PHONE);
            item.setShowDivider(true);

            detailList.add(item);

        }

        //address
        String addressLocationFull = "";
        if (currentPlayfield.getNeighborhood() != null && currentPlayfield.getNeighborhood().getTitle().length() > 0) {
            addressLocationFull += currentPlayfield.getNeighborhood().getTitle();
        }
        if (currentPlayfield.getCity() != null && currentPlayfield.getCity().getTitle().length() > 0) {
            addressLocationFull += ", " + currentPlayfield.getCity().getTitle();
        }
        if (currentPlayfield.getState() != null && currentPlayfield.getState().getTitle().length() > 0) {
            addressLocationFull += ", " + currentPlayfield.getState().getTitle();
        }
        if (currentPlayfield.getCountry() != null && currentPlayfield.getCountry().getTitle().length() > 0) {
            addressLocationFull += ", " + currentPlayfield.getCountry().getTitle();
        }

        if(addressLocationFull.length() > 0) {
            ContactInfoItem item = new ContactInfoItem();
            item.setPrimaryTitle(currentPlayfield.getAddress());
            item.setSecondaryTitle(addressLocationFull);
            item.setPrimaryIcon(getResources().getDrawable(R.drawable.ic_place_black_24px));
            item.setSecondaryIcon(getResources().getDrawable(R.drawable.ic_directions_black_24px));
            item.setType(ContactInfoItem.ItemType.ADDRESS);
            item.setShowDivider(true);
            item.setEntity(currentPlayfield);

            detailList.add(item);
        }

        //contact info
        if(currentPlayfield.getMail() != null) {
            ContactInfoItem item = new ContactInfoItem();
            item.setPrimaryTitle(getString(R.string.contact_info));
            item.setSecondaryTitle(getString(R.string.click_to_contact));
            item.setPrimaryIcon(getResources().getDrawable(R.drawable.ic_email_black_24px));
            item.setType(ContactInfoItem.ItemType.CONTACT_INFO);
            item.setShowDivider(true);
            item.setEntity(currentPlayfield);

            detailList.add(item);
        }

        return detailList;
    }

    private void loadGallery() {
        if(currentPlayfield != null) {
            LinearLayout galleryLayout = (LinearLayout) findViewById(R.id.gallery_layout);

            if(currentPlayfield.getGallery().getImages().size() > 0) {
                for(int i = 0; i < currentPlayfield.getGallery().getImages().size(); i++) {
                    Image image = currentPlayfield.getGallery().getImages().get(i);
                    String imageUrl = image.getUrl();

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
                    layoutParams.setMargins(10, 0, (i == currentPlayfield.getGallery().getImages().size() -1 ? 10 : 0), 10);
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setLayoutParams(layoutParams);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    App.getInstance().getUniversalImageLoader().displayImage(imageUrl, imageView);

                    final int currentImage = i;

                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(PlayfieldDetailActivity.this, GalleryFullScreenActivity.class);
                            intent.putExtra("gallery", (Serializable) currentPlayfield.getGallery());
                            intent.putExtra("current", currentImage);

                            startActivity(intent);
                        }
                    });

                    galleryLayout.addView(imageView);
                }

            } else {
                CardView playfieldGallery = (CardView) findViewById(R.id.playfield_gallery);
                playfieldGallery.setVisibility(View.GONE);
            }
        }
    }

    private void loadInfo() {
        String features = "";

        features += "SERVICIOS: <br>";
        if(currentPlayfield.getFeatures().size() > 0) {
            for(int i=0; i < currentPlayfield.getFeatures().size(); i++) {
                Feature feature = currentPlayfield.getFeatures().get(i);
                features += feature.getTitle() + "<br>";
            }
        }

        final int linesCount = linesCount(features);

        final TextView infoDescription = (TextView) findViewById(R.id.info_description);
        infoDescription.setText(Html.fromHtml(features));

        final Button viewMore = (Button) findViewById(R.id.view_more);
        viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewMore.getText().equals(getString(R.string.view_more))) {
                    viewMore.setText(getString(R.string.view_less));
                    infoDescription.setLines(linesCount);
                } else {
                    viewMore.setText(getString(R.string.view_more));
                    infoDescription.setLines(3);
                }
            }
        });
    }

    private int linesCount(String text) {
        String[] lines = text.split("<br>");
        return  lines.length;
    }

    private void loadFieldList() {
        LinearLayout fieldListLayout = (LinearLayout) findViewById(R.id.field_list_layout);

        if(currentPlayfield.getFields().size() > 0) {
            for(int i=0; i < currentPlayfield.getFields().size(); i++) {
                Field field = currentPlayfield.getFields().get(i);

                View view = View.inflate(this, R.layout.item_field, null);

                if(field.getPlayer() != null) {
                    TextView playersCount = (TextView) view.findViewById(R.id.players_count);
                    playersCount.setText(field.getPlayer().getTitle());
                }

                if(field.getTitle() != null) {
                    TextView title = (TextView) view.findViewById(R.id.field_title);
                    title.setText(field.getTitle());
                }

                if(field.getSoilType() != null) {
                    TextView soilTypeOfPlayfield = (TextView) view.findViewById(R.id.soil_type_of_playfield);
                    soilTypeOfPlayfield.setText(field.getSoilType().getTitle());
                }

                if(field.getCeiling() != null && field.getLight() != null) {
                    TextView lightOfPlayfield = (TextView) view.findViewById(R.id.light_of_playfield);

                    String playfieldDescription = "";

                    if(field.getCeiling()) {
                        playfieldDescription += getString(R.string.indoor_field) + "<br>";
                    } else {
                        playfieldDescription += getString(R.string.outdoor_field) + "<br>";
                    }

                    if(field.getLight()) {
                        playfieldDescription += getString(R.string.with_light);
                    } else {
                        playfieldDescription += getString(R.string.without_light);
                    }

                    lightOfPlayfield.setText(Html.fromHtml(playfieldDescription));
                }

                fieldListLayout.addView(view);
            }
        }
    }

    private void loadPositiveVotes() {
        mPrimaryPositiveVotesCount = (TextView) findViewById(R.id.primary_positive_votes_count);
        mSecondaryPositiveVotesCount = (TextView) findViewById(R.id.secondary_positive_votes_count);

        if(currentPlayfield.getPositiveVotes() != null) {
            mPrimaryPositiveVotesCount.setText(currentPlayfield.getPositiveVotes().toString());
            mSecondaryPositiveVotesCount.setText(currentPlayfield.getPositiveVotes().toString());
        }
    }

    public void initShareButtons() {
        if(mTwitterButton == null) mTwitterButton = (FloatingActionButton) findViewById(R.id.share_twitter_button);
        if(mFacebookButton == null) mFacebookButton = (FloatingActionButton) findViewById(R.id.share_facebook_button);

        mFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inFabebook(PlayfieldDetailActivity.this, currentPlayfield.getDetailUrl());
            }
        });

        mTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inTwitter(PlayfieldDetailActivity.this, currentPlayfield.getDetailUrl());
            }
        });

        mFacebookButton.setColorFilter(Color.argb(255, 255, 255, 255));
        mTwitterButton.setColorFilter(Color.argb(255, 255, 255, 255));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_article_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_article_detail_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, currentPlayfield.getDetailUrl());
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.activity_share_subject));
            startActivity(Intent
                    .createChooser(intent, getResources().getString(R.string.activity_share_title))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(status.equals("success")) {
            currentPlayfield.setPositiveVotes(currentPlayfield.getPositiveVotes()+1);

            mPrimaryPositiveVotesCount.setText(currentPlayfield.getPositiveVotes().toString());
            mSecondaryPositiveVotesCount.setText(currentPlayfield.getPositiveVotes().toString());
        }

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }

    private void addPositiveVotes() {
        if(mPlayfieldService == null) mPlayfieldService = new PlayfieldService("PositiveVotes", this, this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(currentPlayfield.getId()));
        params.put("like", "1");

        mPlayfieldService.post(params);

        ImageView heart = (ImageView) findViewById(R.id.heart);
        AnimatorUtils.heartBeat(heart);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getInstance().trackScreenView("Detalle Canchas - " + currentPlayfield.getTitle() + " - " + currentPlayfield.getId());
    }
}
