package com.p3design.hsj.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.p3design.hsj.R;
import com.p3design.hsj.adapter.RecommendedArticleListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Article;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Gallery;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.ArticleService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.CustomSocialShare;
import com.p3design.hsj.util.YouTubeUrlParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleDetailActivity extends AppCompatActivity implements ServiceObserver, YouTubePlayer.OnInitializedListener {

    private final String SERVICE_ID = "RecommendedArticles";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    private String mYouTubeVideoID = "";
    YouTubePlayerSupportFragment mYouTubePlayerSupportFragment;

    private Article currentArticle;
    private ArticleService mArticleService;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Entity> mArticles = new ArrayList<Entity>();
    private RecommendedArticleListAdapter mArticleListAdapter;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFacebookButton;
    private FloatingActionButton mTwitterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        currentArticle = (Article) getIntent().getSerializableExtra("article");

        final ImageView articleImageView = (ImageView) findViewById(R.id.article_image);
        TextView articleTitleView = (TextView) findViewById(R.id.article_title);
        TextView articleCategoryView = (TextView) findViewById(R.id.article_category);
        TextView articleSummaryView = (TextView) findViewById(R.id.article_summary);
        TextView articleContentView = (TextView) findViewById(R.id.article_content);
        TextView articleCreatedAtView = (TextView) findViewById(R.id.article_created_at);

        if(currentArticle.getFeaturedImage() != null) {
            App.getInstance().getUniversalImageLoader().displayImage(currentArticle.getFeaturedImage().getUrl(), articleImageView);
        }

        articleTitleView.setText(currentArticle.getTitle());
        articleSummaryView.setText(Html.fromHtml(currentArticle.getSummary()));
        articleContentView.setText(Html.fromHtml(getArticleContentProcessed()));
        articleContentView.setMovementMethod(LinkMovementMethod.getInstance());
        articleCategoryView.setText(currentArticle.getCategory());
        articleCreatedAtView.setText(getFormattedDate());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(currentArticle.getCategory());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.AppTheme_Widget_ExpandedAppBar_Titleless);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.AppTheme_Widget_CollapsedAppBar);

        articleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArticleDetailActivity.this, GalleryFullScreenActivity.class);
                Gallery gallery = new Gallery();
                gallery.getImages().add(currentArticle.getFeaturedImage());
                intent.putExtra("gallery", (Serializable) gallery);

                String transitionArticleImage = v.getResources().getString(R.string.transition_id_article_detail_image);

                ActivityOptionsCompat options =
                        ActivityOptionsCompat.makeSceneTransitionAnimation(ArticleDetailActivity.this,
                                Pair.create((View) articleImageView, transitionArticleImage)
                        );
                ActivityCompat.startActivity(ArticleDetailActivity.this, intent, options.toBundle());
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recommended_articles);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mArticleListAdapter = new RecommendedArticleListAdapter(mArticles, this);
        mRecyclerView.setAdapter(mArticleListAdapter);

        mYouTubePlayerSupportFragment = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_view);
        mYouTubePlayerSupportFragment.initialize(App.YOUTUBE_DEVELOPER_KEY, this);

        loadGallery();
        initShareButtons();
        fetchRecommendedArticles();
        initAds();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_article_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_article_detail_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, currentArticle.getDetailUrl());
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.activity_share_subject));
            startActivity(Intent
                    .createChooser(intent, getResources().getString(R.string.activity_share_title))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadGallery() {
        if(currentArticle != null) {
            LinearLayout gallery = (LinearLayout) findViewById(R.id.gallery_layout);

            final Gallery articleGallery = new Gallery();

            if(currentArticle.getImage() != null) {
                Image image = currentArticle.getImage();
                String imageUrl = image.getUrl();
                articleGallery.getImages().add(image);

                ImageView imageView = new ImageView(getApplicationContext());
                imageView.setLayoutParams(new ViewGroup.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                App.getInstance().getUniversalImageLoader().displayImage(imageUrl, imageView);

                final int currentImage = 0;

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ArticleDetailActivity.this, GalleryFullScreenActivity.class);
                        intent.putExtra("gallery", (Serializable) articleGallery);
                        intent.putExtra("current", currentImage);

                        startActivity(intent);
                    }
                });

                gallery.addView(imageView);
            }
        }
    }

    private String getFormattedDate() {
        String formattedDate = "";
        if(currentArticle != null && currentArticle.getCreatedAt() != null) {
            String[] parts = currentArticle.getCreatedAt().split("-");
            if(!parts[1].isEmpty()) {
                switch (parts[1]) {
                    case "01":
                        formattedDate = getString(R.string.january);
                        break;
                    case "02":
                        formattedDate = getString(R.string.february);
                        break;
                    case "03":
                        formattedDate = getString(R.string.march);
                        break;
                    case "04":
                        formattedDate = getString(R.string.april);
                        break;
                    case "05":
                        formattedDate = getString(R.string.may);
                        break;
                    case "06":
                        formattedDate = getString(R.string.june);
                        break;
                    case "07":
                        formattedDate = getString(R.string.july);
                        break;
                    case "08":
                        formattedDate = getString(R.string.august);
                        break;
                    case "09":
                        formattedDate = getString(R.string.september);
                        break;
                    case "10":
                        formattedDate = getString(R.string.october);
                        break;
                    case "11":
                        formattedDate = getString(R.string.november);
                        break;
                    case "12":
                        formattedDate = getString(R.string.december);
                        break;
                }
            }
            if(!parts[0].isEmpty()) {
                formattedDate += " | " + parts[0];
            }
        }
        return formattedDate;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getInstance().cancelPendingRequests(App.TAG);
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mArticles.addAll(entities);
            mArticleListAdapter.notifyDataSetChanged();
        } else {
            TextView recommendedArticlesTitle = (TextView) findViewById(R.id.recommended_articles_title);
            recommendedArticlesTitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }

    private void fetchRecommendedArticles() {
        if (mArticleService == null) mArticleService = new ArticleService(this, this);
        mArticleService.setId(SERVICE_ID);
        int randomPage = 1 + (int)(Math.random() * 3);

        Map<String, String> params = new HashMap<String, String>();
        params.put("category", "14");
        params.put("exclude_featured", "1");
        params.put("limit", "3");
        params.put("page", String.valueOf(randomPage));

        mArticleService.post(params);
    }

    public void initAds() {
        AdView mAdView = (AdView) findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void initShareButtons() {
        if(mTwitterButton == null) mTwitterButton = (FloatingActionButton) findViewById(R.id.share_twitter_button);
        if(mFacebookButton == null) mFacebookButton = (FloatingActionButton) findViewById(R.id.share_facebook_button);

        mFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inFabebook(ArticleDetailActivity.this, currentArticle.getDetailUrl());
            }
        });

        mTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inTwitter(ArticleDetailActivity.this, currentArticle.getDetailUrl());
            }
        });

        mFacebookButton.setColorFilter(Color.argb(255, 255, 255, 255));
        mTwitterButton.setColorFilter(Color.argb(255, 255, 255, 255));
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getInstance().trackScreenView("Detalle Novedades - " + currentArticle.getTitle() + " - " + currentArticle.getId());
    }

    private String getArticleContentProcessed() {
        String content = currentArticle.getContent();
        Document doc = Jsoup.parse(content);

        Elements links = doc.getElementsByTag("a");
        for (Element link : links) {
            String linkHref = link.attr("href");

            if(linkHref != null && !linkHref.contains("http")) {
                content = content.replace(linkHref, App.SITE_URL + "/" + linkHref);
            }
        }

        return content;
    }

    private String getYouTubeVideoUrl() {
        String content = currentArticle.getContent();
        Document doc = Jsoup.parse(content);

        String youtubeUrl = "";

        Elements iframes = doc.getElementsByTag("iframe");
        for (Element iframe : iframes) {
            String linkHref = iframe.attr("src");

            if(linkHref != null && linkHref.contains("youtu")) {
                youtubeUrl = linkHref;
            }
        }

        return youtubeUrl;
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_youtube_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {

            String youTubeVideoUrl = getYouTubeVideoUrl();
            if(youTubeVideoUrl != null) {
                mYouTubeVideoID = YouTubeUrlParser.getVideoId(youTubeVideoUrl);
            }

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            if(mYouTubeVideoID != null && !mYouTubeVideoID.isEmpty()) {
                //player.loadVideo(mYouTubeVideoID);
                player.cueVideo(mYouTubeVideoID);
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            } else {
                View youtubeContainer = (View) findViewById(R.id.youtube_container);
                youtubeContainer.setVisibility(View.GONE);
                //mYouTubePlayerSupportFragment.getView().setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            mYouTubePlayerSupportFragment.initialize(App.YOUTUBE_DEVELOPER_KEY, this);
        }
    }
}
