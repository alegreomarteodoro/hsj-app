package com.p3design.hsj.activity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.ContactFormService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.FormUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class MainContactFormActivity extends DrawerActivity implements ServiceObserver {

    public static final String TAG = MainContactFormActivity.class.getSimpleName();

    private String mTitle;
    private String mDescription;

    private TextView mContactFormTitle;
    private TextView mContactFormDescription;
    private EditText mContactFormName;
    private EditText mContactFormSurname;
    private EditText mContactFormEmail;
    private EditText mContactFormPhone;
    private EditText mContactFormMessage;
    private Button mContactFormSend;

    private ContactFormService mContactFormService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_contact_form);

        mTitle = (String) getIntent().getStringExtra("title");
        mDescription = (String) getIntent().getStringExtra("description");

        mContactFormTitle = (TextView) findViewById(R.id.contact_form_title);
        mContactFormDescription = (TextView) findViewById(R.id.contact_form_description);
        mContactFormName = (EditText) findViewById(R.id.contact_form_name);
        mContactFormSurname = (EditText) findViewById(R.id.contact_form_surname);
        mContactFormEmail = (EditText) findViewById(R.id.contact_form_email);
        mContactFormPhone = (EditText) findViewById(R.id.contact_form_phone);
        mContactFormMessage = (EditText) findViewById(R.id.contact_form_message);
        mContactFormSend = (Button) findViewById(R.id.send_form);

        if(mTitle != null && !mTitle.isEmpty()) {
            mContactFormTitle.setText(mTitle);
        }

        if(mDescription != null && !mDescription.isEmpty()) {
            mContactFormDescription.setText(Html.fromHtml(mDescription));
        }

        mContactFormSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendForm();
            }
        });

        mToolbar.setTitle(getString(R.string.contact));
        mNavigationView.getMenu().getItem(3).setChecked(true);

        initAds();
    }

    private void sendForm() {
        boolean error = false;

        if( mContactFormName.getText().toString().trim().equals("")){
            mContactFormName.setError( "El nombre es requerido" );
            error = true;
        }
        if( mContactFormSurname.getText().toString().trim().equals("")){
            mContactFormSurname.setError( "El apellido es requerido" );
            error = true;
        }
        if( mContactFormEmail.getText().toString().trim().equals("")){
            mContactFormEmail.setError( "El email es requerido" );
            error = true;
        } else if(!FormUtils.isEmailValid(mContactFormEmail.getText().toString().trim())){
            mContactFormEmail.setError( "El email no es válido." );
            error = true;
        }
        if( mContactFormPhone.getText().toString().trim().equals("")){
            mContactFormPhone.setError( "El teléfono es requerido" );
            error = true;
        }
        if( mContactFormMessage.getText().toString().trim().equals("")){
            mContactFormMessage.setError( "Ingrese su comentario." );
            error = true;
        }

        if(error) {
            return;
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("save", "1");
        params.put("name", mContactFormName.getText().toString());
        params.put("surname", mContactFormSurname.getText().toString());
        params.put("email", mContactFormEmail.getText().toString());
        params.put("phone", mContactFormPhone.getText().toString());
        params.put("comments", mContactFormMessage.getText().toString());

        if( mContactFormService == null) mContactFormService = new ContactFormService(this, this);
        mContactFormService.post(params);
    }

    public void initAds() {
        AdView mAdView = (AdView) findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        if(status.equals("success")) {
            mContactFormName.setText("");
            mContactFormSurname.setText("");
            mContactFormEmail.setText("");
            mContactFormPhone.setText("");
            mContactFormMessage.setText("");
        }
    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }
}
