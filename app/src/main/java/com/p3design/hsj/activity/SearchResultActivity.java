package com.p3design.hsj.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.TopRatedPlayfieldListAdapter;
import com.p3design.hsj.adapter.TopRatedTournamentListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.PlayfieldService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.service.TournamentService;
import com.p3design.hsj.util.DynamicView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class SearchResultActivity extends AppCompatActivity implements ServiceObserver{

    public static final String SERVICE_PLAYFIELD_ID = "PlayfieldList";
    public static final String SERVICE_TOURNAMENT_ID = "TournamentList";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Entity> mItems = new ArrayList<Entity>();

    private TournamentService mTournamentService;
    private PlayfieldService mPlayfieldService;

    private TopRatedPlayfieldListAdapter mTopRatedPlayfieldListAdapter;
    private TopRatedTournamentListAdapter mTopRatedTournamentListAdapter;

    private int mSearchContext;
    private Map<String, String> mCurrentSearchParams = new HashMap<String, String>();

    DynamicView mDynamicView;
    View mConnectionUnavailableLayout;
    View mConnectionUnavailableInListLayout;
    View mLoadingLayout;
    View mNotMatchResultsLayout;

    private int mPagination = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSearchContext = getIntent().getIntExtra("searchContext", SearchFormActivity.SearchContext.PLAYFIELDS);
        mCurrentSearchParams = SearchFormActivity.getSearchParamsInstance().getFilters();

        mDynamicView = App.newActivityDynamicView(this, R.layout.activity_search_result);

        mConnectionUnavailableInListLayout = mDynamicView.getView(App.ERROR_CONNECTION_IN_LIST_LAYOUT);
        mConnectionUnavailableLayout = mDynamicView.getView(App.ERROR_CONNECTION_LAYOUT);
        mLoadingLayout = mDynamicView.getView(App.LOADING_LAYOUT);
        mNotMatchResultsLayout = mDynamicView.getView(App.NOT_MATCH_RESULTS_LAYOUT);

        RelativeLayout.LayoutParams parentBottomLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        parentBottomLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        parentBottomLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

        RelativeLayout.LayoutParams centerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        centerLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

        mConnectionUnavailableInListLayout.setLayoutParams(parentBottomLayoutParams);
        mLoadingLayout.setLayoutParams(parentBottomLayoutParams);
        mConnectionUnavailableLayout.setLayoutParams(centerLayoutParams);

        mRecyclerView = (RecyclerView) findViewById(R.id.search_result);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int itemsCount = mItems.size() - 1;
                int lastItem = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (itemsCount == lastItem) {
                    fetchItems();
                }
            }
        });

        if(mSearchContext == SearchFormActivity.SearchContext.PLAYFIELDS) {

            mTopRatedPlayfieldListAdapter = new TopRatedPlayfieldListAdapter(mItems, this);
            mRecyclerView.setAdapter(mTopRatedPlayfieldListAdapter);

            fetchPlayfields();

        } else if (mSearchContext == SearchFormActivity.SearchContext.TOURNAMENT) {

            mTopRatedTournamentListAdapter = new TopRatedTournamentListAdapter(mItems, this);
            mRecyclerView.setAdapter(mTopRatedTournamentListAdapter);

            fetchTournaments();

        }

        Button retryButton = (Button) mConnectionUnavailableLayout.findViewById(R.id.retry);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchItems();
            }
        });

        Button retryButton2 = (Button) mConnectionUnavailableInListLayout.findViewById(R.id.retry);
        retryButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchItems();
            }
        });

        Button goBackButton = (Button) mNotMatchResultsLayout.findViewById(R.id.go_back);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setTitle(getString(R.string.search_result_title));
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreyActionBar)));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        changeStatusBarColor();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchPlayfields() {
        if(mPlayfieldService == null) {
            mPlayfieldService = new PlayfieldService(SERVICE_PLAYFIELD_ID, this, this);
            mPlayfieldService.disabledProgressDialog();
        }

        mCurrentSearchParams.put("page", String.valueOf(mPagination));
        mPlayfieldService.post(mCurrentSearchParams);

        mPagination++;
    }

    private void fetchTournaments() {
        if(mTournamentService == null) {
            mTournamentService = new TournamentService(SERVICE_TOURNAMENT_ID, this, this);
            mTournamentService.disabledProgressDialog();
        }

        mCurrentSearchParams.put("page", String.valueOf(mPagination));
        mCurrentSearchParams.put("limit", "10");
        mTournamentService.post(mCurrentSearchParams);

        mPagination++;
    }

    private void fetchItems() {
        if(mSearchContext == SearchFormActivity.SearchContext.PLAYFIELDS) {
            fetchPlayfields();
        } else if (mSearchContext == SearchFormActivity.SearchContext.TOURNAMENT) {
            fetchTournaments();
        }
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mItems.addAll(entities);

            if(currentServiceInstance.getId() == SERVICE_PLAYFIELD_ID) mTopRatedPlayfieldListAdapter.notifyDataSetChanged();
            if(currentServiceInstance.getId() == SERVICE_TOURNAMENT_ID) mTopRatedTournamentListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onErrorOcurred(Exception e) {
        if(mItems.size() == 0) {
            mPagination = 1;
            mDynamicView.showView(App.ERROR_CONNECTION_LAYOUT);
        } else {
            mDynamicView.getView(App.LOADING_LAYOUT).setVisibility(View.GONE);
            mDynamicView.showView(App.ERROR_CONNECTION_IN_LIST_LAYOUT, DynamicView.VisibilityType.MULTIPLE);
        }
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(Service.COMPLETED.equals(status)) {

            if(mItems.size() > 0) mDynamicView.showView(App.MAIN_LAYOUT);
            else mDynamicView.showView(App.NOT_MATCH_RESULTS_LAYOUT);

        } else if(Service.LOADING.equals(status)) {

            mDynamicView.getView(App.ERROR_CONNECTION_IN_LIST_LAYOUT).setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);

            if(mItems.size() == 0) {
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            } else  {
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            }

            mLoadingLayout.setLayoutParams(layoutParams);
            mLoadingLayout.setVisibility(View.VISIBLE);

        }
    }

    private void changeStatusBarColor() {
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorGreyStatusBar));
        }
    }
}
