package com.p3design.hsj.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.adapter.ArticleListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.ArticleService;
import com.p3design.hsj.service.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 15/1/16.
 */
public class MainActivity extends DrawerActivity implements ServiceObserver, SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = MainActivity.class.getSimpleName();

    private SwipeRefreshLayout mSwipeRefreshLayout;
    View mConnectionUnavailableLayout;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<Entity> mArticles = new ArrayList<Entity>();
    private ArticleListAdapter mArticleListAdapter;
    private ArticleService mArticleService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.article_list);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mConnectionUnavailableLayout = (View) findViewById(R.id.connection_unavailable);
        mConnectionUnavailableLayout.setVisibility(View.GONE);

        mArticleListAdapter = new ArticleListAdapter(mArticles, this);
        mRecyclerView.setAdapter(mArticleListAdapter);

        mArticleService = new ArticleService(this, this);
        mArticleService.disabledProgressDialog();

        mSwipeRefreshLayout.setOnRefreshListener(this);

        mSwipeRefreshLayout.post(new Runnable() {
                                     @Override
                                     public void run() {
                                         mSwipeRefreshLayout.setRefreshing(true);
                                         fetchArticles();
                                     }
                                 }
        );

        mNavigationView.getMenu().getItem(0).setChecked(true);
        mToolbar.setTitle(getString(R.string.menu_news));

        Button retryButton = (Button) mConnectionUnavailableLayout.findViewById(R.id.retry);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRefresh();
            }
        });

        initAds();
    }

    @Override
    public void onRefresh() {
        clearArticles();
        clearCache();
        fetchArticles();
    }

    private void fetchArticles() {
        mArticleService.get("/limit/10");
    }

    public void clearArticles() {
        mArticles.clear();
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mArticles.addAll(entities);
            mArticleListAdapter.notifyDataSetChanged();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(status.equals(Service.FAILED)) {
            mConnectionUnavailableLayout.setVisibility (View.VISIBLE);
        } else {
            mConnectionUnavailableLayout.setVisibility(View.GONE);
        }
    }

    public void initAds() {
        AdView mAdView = (AdView) findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void clearCache() {
        mArticleService.clearServiceCache(true);
    }
}