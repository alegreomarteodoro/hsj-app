package com.p3design.hsj.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Playfield;
import com.p3design.hsj.entity.Tournament;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.ContactFormService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.FormUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 16/2/16.
 */
public class ContactFormActivity extends AppCompatActivity implements ServiceObserver {

    private String mTitle;
    private String mDescription;
    private String mContactEmail;
    private Entity mEntity;
    private String mEntityId;
    private String mEntityName;
    private String mEntityType;

    private TextView mContactFormTitle;
    private TextView mContactFormDescription;
    private EditText mContactFormName;
    private EditText mContactFormSurname;
    private EditText mContactFormEmail;
    private EditText mContactFormPhone;
    private EditText mContactFormMessage;
    private Button mContactFormSend;

    private ContactFormService mContactFormService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_form);

        mTitle = (String) getIntent().getStringExtra("title");
        mDescription = (String) getIntent().getStringExtra("description");
        mEntity = (Entity) getIntent().getSerializableExtra("entity");
        if(mEntity != null) {
            if(mEntity instanceof Playfield) {
                Playfield playfield = (Playfield) mEntity;
                mContactEmail = playfield.getMail();
                mEntityId  = String.valueOf(playfield.getId());
                mEntityName = playfield.getTitle();
                mEntityType = "Complejos";
            } else {
                Tournament tournament = (Tournament) mEntity;
                mContactEmail = tournament.getMail();
                mEntityId = String.valueOf(tournament.getId());
                mEntityName = tournament.getTitle();
                mEntityType = "Torneos";
            }
        }

        mContactFormTitle = (TextView) findViewById(R.id.contact_form_title);
        mContactFormDescription = (TextView) findViewById(R.id.contact_form_description);
        mContactFormName = (EditText) findViewById(R.id.contact_form_name);
        mContactFormSurname = (EditText) findViewById(R.id.contact_form_surname);
        mContactFormEmail = (EditText) findViewById(R.id.contact_form_email);
        mContactFormPhone = (EditText) findViewById(R.id.contact_form_phone);
        mContactFormMessage = (EditText) findViewById(R.id.contact_form_message);
        mContactFormSend = (Button) findViewById(R.id.send_form);

        if(mTitle != null && !mTitle.isEmpty()) {
            mContactFormTitle.setText(mTitle);
        }

        if(mDescription != null && !mDescription.isEmpty()) {
            mContactFormDescription.setText(Html.fromHtml(mDescription));
        }

        mContactFormSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendForm();
            }
        });

        //toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        getSupportActionBar().setTitle(getString(R.string.contact));

        initAds();
    }

    private void sendForm() {
        boolean error = false;

        if( mContactFormName.getText().toString().trim().equals("")){
            mContactFormName.setError( "El nombre es requerido" );
            error = true;
        }
        if( mContactFormSurname.getText().toString().trim().equals("")){
            mContactFormSurname.setError( "El apellido es requerido" );
            error = true;
        }
        if( mContactFormEmail.getText().toString().trim().equals("")){
            mContactFormEmail.setError( "El email es requerido" );
            error = true;
        } else if(!FormUtils.isEmailValid(mContactFormEmail.getText().toString().trim())){
            mContactFormEmail.setError( "El email no es válido." );
            error = true;
        }
        if( mContactFormPhone.getText().toString().trim().equals("")){
            mContactFormPhone.setError( "El teléfono es requerido" );
            error = true;
        }
        if( mContactFormMessage.getText().toString().trim().equals("")){
            mContactFormMessage.setError( "Ingrese su comentario." );
            error = true;
        }

        if(error) {
            return;
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("save", "1");
        params.put("name", mContactFormName.getText().toString());
        params.put("surname", mContactFormSurname.getText().toString());
        params.put("email", mContactFormEmail.getText().toString());
        params.put("phone", mContactFormPhone.getText().toString());
        params.put("comments", mContactFormMessage.getText().toString());

        if(mContactEmail != null) {
            params.put("contact_email", mContactEmail);
        }
        if(mEntityId != null) {
            params.put("parent_id", mEntityId);
        }
        if(mEntityName != null) {
            params.put("parent_name", mEntityName);
        }
        if(mEntityType != null) {
            params.put("parent_type", mEntityType);
        }

        if( mContactFormService == null) mContactFormService = new ContactFormService(this, this);
        mContactFormService.post(params);

        String trackLabel = "";
        String trackCategory = "";

        if(mEntity != null) {
            if(mEntity instanceof Playfield) {
                trackCategory = "Canchas";
            } else {
                trackCategory = "Torneos";
            }

            trackLabel = mEntity.getTitle() + " - " + mEntity.getId();
        }

        App.getInstance().trackEvent(trackCategory, "Envio de Formulario de Contacto", trackLabel);
    }

    public void initAds() {
        AdView mAdView = (AdView) findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        if(status.equals("success")) {
            mContactFormName.setText("");
            mContactFormSurname.setText("");
            mContactFormEmail.setText("");
            mContactFormPhone.setText("");
            mContactFormMessage.setText("");
        }
    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }
}
