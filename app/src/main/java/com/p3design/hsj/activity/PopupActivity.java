package com.p3design.hsj.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class PopupActivity extends Activity {

    int widthPixels = 0;
    int heightPixels = 0;
    double widthPercentaje = 0.8;
    double heightPercentaje = 0.7;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initDisplayMetrics();
    }

    private void initDisplayMetrics() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        if(widthPixels == 0) widthPixels = displayMetrics.widthPixels;
        if(heightPixels == 0) heightPixels = displayMetrics.heightPixels;

        getWindow().setLayout((int) (widthPixels * widthPercentaje), (int) (heightPixels * heightPercentaje));
    }

    public void setDisplayMetrics(int widthPixels, int heightPixels) {
        this.widthPixels = widthPixels;
        this.heightPixels = heightPixels;
    }

    public void setDisplayMetrics(int widthPixels, int heightPixels, double widthPercentaje, double heightPercentaje) {
        this.widthPixels = widthPixels;
        this.heightPixels = heightPixels;
        this.widthPercentaje = widthPercentaje;
        this.heightPercentaje = heightPercentaje;
    }

    public void setDisplayMetrics(double widthPercentaje, double heightPercentaje) {
        this.widthPercentaje = widthPercentaje;
        this.heightPercentaje = heightPercentaje;
    }
}
