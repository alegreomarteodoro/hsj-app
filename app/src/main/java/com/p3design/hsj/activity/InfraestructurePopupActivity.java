package com.p3design.hsj.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.CheckListAdapter;
import com.p3design.hsj.entity.CheckableItem;
import com.p3design.hsj.util.SearchParams;

import java.util.ArrayList;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public class InfraestructurePopupActivity extends PopupActivity {

    private Button mCancelButton;
    private Button mOkButton;

    private ListView mInfraestructureList;
    private CheckListAdapter mCheckListAdapter;
    private ArrayList<CheckableItem> mItems = new ArrayList<CheckableItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDisplayMetrics(0.8, 0.4);
        setContentView(R.layout.activity_infraestructure_popup);

        mItems.add(new CheckableItem(1, getString(R.string.indoor_field)));
        mItems.add(new CheckableItem(0, getString(R.string.outdoor_field)));

        mInfraestructureList = (ListView) findViewById(R.id.infrastructure);
        mCheckListAdapter = new CheckListAdapter(this, mItems);
        mInfraestructureList.setAdapter(mCheckListAdapter);

        setupOkButtonListeners();
        setupCancelButtonListeners();
        setupCheckedAll();
    }

    private void setupCancelButtonListeners() {
        if(mCancelButton == null) mCancelButton = (Button) findViewById(R.id.cancel);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfraestructurePopupActivity.this.finish();
            }
        });
    }

    private void setupOkButtonListeners() {
        if(mOkButton == null) mOkButton = (Button) findViewById(R.id.ok);

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ids = "";
                String values = "";
                ArrayList<CheckableItem> itemsSelected = getItemsSelected();

                for (int i = 0; i < itemsSelected.size(); i++) {
                    CheckableItem checkableItem = itemsSelected.get(i);
                    if (checkableItem.isChecked()) {
                        ids += String.valueOf(checkableItem.getId());
                        if (i != (itemsSelected.size() - 1)) ids += ",";
                        values += String.valueOf(checkableItem.getTitle());
                        if (i != (itemsSelected.size() - 1)) values += ", ";
                    }
                }

                if (!ids.isEmpty() && !values.isEmpty()) {
                    SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.INFRAESTRUCTURE_KEY, ids, values);
                }

                finish();
            }
        });
    }

    private ArrayList<CheckableItem> getItemsSelected() {
        ArrayList<CheckableItem> checkableItems = new ArrayList<CheckableItem>();
        for(int i = 0; i < mItems.size(); i++) {
            CheckableItem item = mItems.get(i);
            if(item.isChecked()) {
                checkableItems.add(item);
            }
        }
        return checkableItems;
    }

    private void setupCheckedAll() {
        final CheckBox checkedAll = (CheckBox) findViewById(R.id.checked_all);
        checkedAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckListAdapter.checkedAllItems();
            }
        });
    }
}