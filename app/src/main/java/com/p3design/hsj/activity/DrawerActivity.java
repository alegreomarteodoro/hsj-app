package com.p3design.hsj.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.p3design.hsj.R;

import java.io.Serializable;

/**
 * Created by omarteodoroalegre on 19/1/16.
 */
public class DrawerActivity extends AppCompatActivity {

    public static final String TAG = DrawerActivity.class.getSimpleName();

    protected Toolbar mToolbar;
    protected ActionBar mActionBar;
    protected DrawerLayout mDrawerLayout;
    protected NavigationView mNavigationView;
    protected ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_drawer);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if(mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Helper method that can be used by child classes to
     * specify that they don't want a {@link Toolbar}
     * @return true
     */
    protected boolean useToolbar() {
        return true;
    }


    /**
     * Helper method to allow child classes to opt-out of having the
     * hamburger menu.
     * @return true
     */
    protected boolean useDrawerToggle() {
        return true;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initToolbar();
        setupNavDrawer();
    }

    protected Toolbar initToolbar() {

        if (mToolbar == null) {
            // Initializing Toolbar and setting it as the actionbar
            mToolbar = (Toolbar) findViewById(R.id.toolbar);

            if(mToolbar != null) {
                if(useToolbar()) {
                    setSupportActionBar(mToolbar);
                } else {
                    mToolbar.setVisibility(View.GONE);
                }
            }
        }

        return mToolbar;
    }

    private void setupNavDrawer() {


        //Initializing NavigationView
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout == null) {
            return;
        }

        //Initializing NavigationView
        mNavigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        if(mNavigationView == null) {
            return;
        }

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                FragmentManager fragmentManager = getSupportFragmentManager();

                menuItem.setChecked(true);

                //Closing drawer on item click
                mDrawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {

                    case R.id.news_menu:
                        createBackStack(new Intent(getApplicationContext(), MainActivity.class));
                        return true;

                    case R.id.playfields_menu:
                        createBackStack(new Intent(getApplicationContext(), PlayfieldListActivity.class));
                        return true;

                    case R.id.tournaments_menu:
                        createBackStack(new Intent(getApplicationContext(), TournamentListActivity.class));
                        return true;

                    case R.id.contact_menu:
                        launchContactFormActivity();
                        return true;

                    case R.id.twitter_menu:
                        launchFollowMe("https://twitter.com/hoysejuega");
                        return true;

                    case R.id.instagram_menu:
                        launchFollowMe("https://www.instagram.com/hoysejuega/");
                        return true;

                    case R.id.facebook_menu:
                        launchFollowMe("https://web.facebook.com/sejuega/");
                        return true;

                    default:
                        return true;

                }
            }
        });

        if( useDrawerToggle()) {
            mDrawerToggle = new ActionBarDrawerToggle(
                    this,
                    mDrawerLayout,
                    mToolbar,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close
            ) {

                /**
                 * Called when a drawer has settled in a completely closed state.
                 */
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                }

                /**
                 * Called when a drawer has settled in a completely open state.
                 */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }
            };
        }

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mActionBar = getSupportActionBar();
        if(mActionBar != null && useToolbar()) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setTitle(getString(R.string.app_name));
        }
    }


    /**
     * Enables back navigation for activities that are launched from the NavBar. See
     * {@code AndroidManifest.xml} to find out the parent activity names for each activity.
     * @param intent
     */
    private void createBackStack(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            TaskStackBuilder builder = TaskStackBuilder.create(this);
            builder.addNextIntentWithParentStack(intent);
            builder.startActivities();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        }
    }

    private void launchContactFormActivity() {

        Intent intent = new Intent(getApplicationContext(), MainContactFormActivity.class);
        intent.putExtra("title", getString(R.string.contact_form_title1));
        intent.putExtra("email", "marketing@hoysejuega.com");
        intent.putExtra("description", getString(R.string.contact_form_description1));

        createBackStack(intent);
    }

    private void launchFollowMe(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

}
