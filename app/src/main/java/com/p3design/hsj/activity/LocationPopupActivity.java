package com.p3design.hsj.activity;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.SpinnerAdapter;
import com.p3design.hsj.entity.City;
import com.p3design.hsj.entity.Country;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Neighborhood;
import com.p3design.hsj.entity.State;
import com.p3design.hsj.location.LocationTracker;
import com.p3design.hsj.observer.LocationObserver;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.CityService;
import com.p3design.hsj.service.CountryService;
import com.p3design.hsj.service.NeighborhoodService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.service.StateService;
import com.p3design.hsj.util.SearchParams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by omarteodoroalegre on 24/2/16.
 */
public class LocationPopupActivity extends PopupActivity implements ServiceObserver, LocationObserver{

    private Button mCancelButton;
    private Button mOkButton;

    private Spinner mCountrySpinner;                 //pais
    private Spinner mStateSpinner;                   //provincia
    private Spinner mCitySpinner;                    //localidad
    private Spinner mNeighnorhoodSpinner;            //barrio
    private CheckBox mCurrentLocationCheckbox;

    private ProgressBar mProgressBar;

    private SpinnerAdapter mCountryAdapter;
    private SpinnerAdapter mStateAdapter;
    private SpinnerAdapter mCityAdapter;
    private SpinnerAdapter mNeighborhoodAdapter;

    private ArrayList<Entity> countries = new ArrayList<Entity>();
    private ArrayList<Entity> states = new ArrayList<Entity>();
    private ArrayList<Entity> cities = new ArrayList<Entity>();
    private ArrayList<Entity> neighborhoods = new ArrayList<Entity>();

    private CountryService mCountryService;
    private StateService mStateService;
    private CityService mCityService;
    private NeighborhoodService mNeighborhoodService;

    private LocationTracker mLocationTracker;
    private Location mLastLocation;
    private String mCurrentLocality = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_location_popup);
        initProgressBar();

        setupCountrySpinnerListener();
        setupStateSpinnerListener();
        setupCitySpinnerListener();
        setupNeighborhoodSpinnerListener();

        initCountryService();

        setupCancelButtonListeners();
        setupOkButtonListeners();

        checkLocation();
        setupCurrentLocationCheckbox();
    }

    private void initProgressBar() {
        if(mProgressBar == null) mProgressBar = (ProgressBar) findViewById(R.id.loading);
    }

    private void initCountryService() {
        if(mCountryService == null) mCountryService = new CountryService(this, this);
        if(mStateService == null) mStateService = new StateService(this, this);
        if(mCityService == null) mCityService = new CityService(this, this);
        if(mNeighborhoodService == null) mNeighborhoodService = new NeighborhoodService(this, this);

        String defaultParams = "/limit/9999";

        mCountryService.disabledProgressDialog();
        mCountryService.get(defaultParams);
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            if (currentServiceInstance instanceof CountryService) {
                countries.addAll(entities);
                mCountryAdapter.notifyDataSetChanged();
            }
            if (currentServiceInstance instanceof StateService) {
                states.addAll(entities);
                mStateAdapter.notifyDataSetChanged();
            }
            if (currentServiceInstance instanceof CityService) {

                cities.addAll(entities);
                mCityAdapter.notifyDataSetChanged();
            }
            if (currentServiceInstance instanceof NeighborhoodService) {
                neighborhoods.addAll(entities);
                mNeighborhoodAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {
        String error = e.getMessage();
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(status.equals(Service.LOADING)) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    public void initCountryList() {
        countries.clear();
        countries.add(new Country(0, "Pais"));
    }

    public void initStateList() {
        states.clear();
        states.add(new State(0, "Provincia"));
    }

    public void initCityList() {
        cities.clear();
        cities.add(new City(0, "Localidad"));
    }

    public void initNeighborhoodList() {
        neighborhoods.clear();
        neighborhoods.add(new Neighborhood(0, "Barrio"));
    }

    public void setupCountrySpinnerListener() {
        initCountryList();

        if(mCountrySpinner == null) mCountrySpinner = (Spinner) findViewById(R.id.country);
        mCountryAdapter = new SpinnerAdapter(this, countries);
        mCountrySpinner.setAdapter(mCountryAdapter);

        mCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                initStateList();
                if (position == 0) {
                    mStateSpinner.setEnabled(false);
                    //SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.COUNTRY_KEY);
                    clearLocationParams();
                } else {
                    mStateSpinner.setEnabled(true);

                    Country currentCountry = (Country) parent.getSelectedItem();

                    mStateService = new StateService(LocationPopupActivity.this, LocationPopupActivity.this);
                    mStateService.disabledProgressDialog();
                    mStateService.get("/pais/" + currentCountry.getId() + "/limit/9999");
                }
                mStateSpinner.setSelection(0);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void setupStateSpinnerListener() {
        initStateList();

        if(mStateSpinner == null) mStateSpinner = (Spinner) findViewById(R.id.state);
        mStateAdapter = new SpinnerAdapter(this, states);
        mStateSpinner.setAdapter(mStateAdapter);

        mStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                initCityList();
                if (position == 0) {
                    mCitySpinner.setEnabled(false);
                    SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.STATE_KEY);
                } else {
                    mCitySpinner.setEnabled(true);

                    State currentState = (State) parent.getSelectedItem();

                    mCityService = new CityService(LocationPopupActivity.this, LocationPopupActivity.this);
                    mCityService.disabledProgressDialog();
                    mCityService.get("/provincia/" + currentState.getId() + "/limit/9999");
                }
                mCitySpinner.setSelection(0);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void setupCitySpinnerListener() {
        initCityList();

        if(mCitySpinner == null) mCitySpinner = (Spinner) findViewById(R.id.city);
        mCityAdapter = new SpinnerAdapter(this, cities);
        mCitySpinner.setAdapter(mCityAdapter);

        mCitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                initNeighborhoodList();
                if (position == 0) {
                    mNeighnorhoodSpinner.setEnabled(false);
                    SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.CITY_KEY);
                } else {
                    mNeighnorhoodSpinner.setEnabled(true);

                    City currentCity = (City) parent.getSelectedItem();

                    mNeighborhoodService = new NeighborhoodService(LocationPopupActivity.this, LocationPopupActivity.this);
                    mNeighborhoodService.disabledProgressDialog();
                    mNeighborhoodService.get("/city/" + currentCity.getId() + "/limit/9999");
                }
                mNeighnorhoodSpinner.setSelection(0);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void setupNeighborhoodSpinnerListener() {
        initNeighborhoodList();

        if(mNeighnorhoodSpinner == null) mNeighnorhoodSpinner = (Spinner) findViewById(R.id.neighnorhood);
        mNeighborhoodAdapter = new SpinnerAdapter(this, neighborhoods);
        mNeighnorhoodSpinner.setAdapter(mNeighborhoodAdapter);

        mNeighnorhoodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.NEIGHBORHOOD_KEY);
                } else {
                    Neighborhood currentNeighborhood = (Neighborhood) parent.getSelectedItem();
                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setupCancelButtonListeners() {
        if(mCancelButton == null) mCancelButton = (Button) findViewById(R.id.cancel);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationPopupActivity.this.finish();
            }
        });
    }

    private void setupOkButtonListeners() {
        if(mOkButton == null) mOkButton = (Button) findViewById(R.id.ok);

        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCurrentLocationCheckbox.isChecked()) {
                    SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.LOCATION_KEY, mLastLocation, mCurrentLocality);
                } else {
                    if(mCountrySpinner.getSelectedItemPosition() != 0) {
                        clearLocationParams();

                        Country countrySelected = (Country) countries.get( mCountrySpinner.getSelectedItemPosition() );
                        SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.COUNTRY_KEY, countrySelected.getId(), countrySelected.getTitle());
                    }

                    if(mStateSpinner.getSelectedItemPosition() != 0) {
                        State stateSelected = (State) states.get( mStateSpinner.getSelectedItemPosition() );
                        SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.STATE_KEY, stateSelected.getId(), stateSelected.getTitle());
                    }

                    if(mCitySpinner.getSelectedItemPosition() != 0) {
                        City citySelected = (City) cities.get( mCitySpinner.getSelectedItemPosition() );
                        SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.CITY_KEY, citySelected.getId(), citySelected.getTitle());
                    }

                    if(mNeighnorhoodSpinner.getSelectedItemPosition() != 0) {
                        Neighborhood neighborhoodSelected = (Neighborhood) neighborhoods.get( mNeighnorhoodSpinner.getSelectedItemPosition() );
                        SearchFormActivity.getSearchParamsInstance().addParam(SearchParams.NEIGHBORHOOD_KEY, neighborhoodSelected.getId(), neighborhoodSelected.getTitle());
                    }
                }

                finish();
            }
        });
    }

    private void setupCurrentLocationCheckbox() {

        if(mLastLocation != null) {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    mCurrentLocality = listAddresses.get(0).getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(mCurrentLocationCheckbox == null) mCurrentLocationCheckbox = (CheckBox) findViewById(R.id.current_location);

        TextView errorLocationMessage = (TextView) findViewById(R.id.error_location_message);

        if(!mCurrentLocality.isEmpty()) {
            mCurrentLocationCheckbox.setText(getString(R.string.my_location) + " (" + mCurrentLocality + ")");
            mCurrentLocationCheckbox.setChecked(true);
            mCurrentLocationCheckbox.setVisibility(View.VISIBLE);
            mCountrySpinner.setEnabled(false);
            errorLocationMessage.setText(Html.fromHtml(getString(R.string.location_message)));
        } else {
            mCountrySpinner.setEnabled(true);
            mCurrentLocationCheckbox.setVisibility(View.GONE);
            errorLocationMessage.setText(Html.fromHtml(getString(R.string.error_location_message)));
        }

        mCurrentLocationCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCurrentLocationCheckbox.isChecked()) {
                    mCountrySpinner.setEnabled(false);
                    mCountrySpinner.setSelection(0);
                } else {
                    mCountrySpinner.setEnabled(true);
                }
            }
        });
    }

    private boolean checkLocation() {
        if(mLocationTracker == null) mLocationTracker = new LocationTracker(this, this);
        if(mLastLocation == null) mLastLocation = mLocationTracker.getLastLocation();
        if(mLastLocation != null) return true;
        return false;
    }

    private void clearLocationParams() {
        SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.COUNTRY_KEY);
        SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.LOCATION_KEY);
        SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.CITY_KEY);
        SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.STATE_KEY);
        SearchFormActivity.getSearchParamsInstance().removeParam(SearchParams.NEIGHBORHOOD_KEY);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
