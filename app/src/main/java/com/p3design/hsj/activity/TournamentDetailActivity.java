package com.p3design.hsj.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.p3design.hsj.R;
import com.p3design.hsj.adapter.ContactInfoAdapter;
import com.p3design.hsj.adapter.HeadquarterListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.ContactInfoItem;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.entity.Feature;
import com.p3design.hsj.entity.Headquarter;
import com.p3design.hsj.entity.Image;
import com.p3design.hsj.entity.Tournament;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.service.TournamentService;
import com.p3design.hsj.util.AnimatorUtils;
import com.p3design.hsj.util.CustomSocialShare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class TournamentDetailActivity extends AppCompatActivity implements ServiceObserver {

    private Tournament currentTournament;
    private RecyclerView mmContactInfoList;
    private ContactInfoAdapter mContactInfoAdapter;
    private RecyclerView mHeadquarterList;
    private HeadquarterListAdapter mHeadquarterListAdapter;

    private FloatingActionButton mFacebookButton;
    private FloatingActionButton mTwitterButton;

    private CardView mCardviewPositiveVote;
    TextView mPrimaryPositiveVotesCount;
    TextView mSecondaryPositiveVotesCount;

    private TournamentService mTournamentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_detail);

        currentTournament = (Tournament) getIntent().getSerializableExtra("tournament");

        final ImageView tournamentImageView = (ImageView) findViewById(R.id.tournament_featured_image);
        TextView tournamentTitle = (TextView) findViewById(R.id.tournament_title);

        tournamentTitle.setText(currentTournament.getTitle());

        tournamentImageView.setImageResource(R.drawable.default_img);

        if(currentTournament.getFeaturedImage() != null && !currentTournament.getFeaturedImage().getUrl().equals(App.SITE_URL)) {
            App.getInstance().getUniversalImageLoader().displayImage(currentTournament.getFeaturedImage().getUrl(), tournamentImageView);
        } else if(currentTournament.getImage() != null) {
            Image imageGallery = currentTournament.getImage();
            App.getInstance().getUniversalImageLoader().displayImage(imageGallery.getUrl(), tournamentImageView);
        }

        //detail list setup
        mmContactInfoList = (RecyclerView) findViewById(R.id.contact_info_list);
        mContactInfoAdapter = new ContactInfoAdapter(loadContactInfo(), this);
        mmContactInfoList.setAdapter(mContactInfoAdapter);
        mmContactInfoList.setLayoutManager(new LinearLayoutManager(this));
        mmContactInfoList.setNestedScrollingEnabled(false);

        mHeadquarterList = (RecyclerView) findViewById(R.id.headquarter_list);
        mHeadquarterListAdapter = new HeadquarterListAdapter(loadHeadquearters(), this);
        mHeadquarterList.setAdapter(mHeadquarterListAdapter);
        mHeadquarterList.setLayoutManager(new LinearLayoutManager(this));
        mHeadquarterList.setNestedScrollingEnabled(false);

        loadInfo();
        loadPositiveVotes();
        loadWebSite();
        setupMap();

        mCardviewPositiveVote = (CardView) findViewById(R.id.cardview_positive_vote);
        mCardviewPositiveVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPositiveVotes();
            }
        });


        //toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //collapsingtoolbarlayout setup
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(currentTournament.getTitle());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.AppTheme_Widget_ExpandedAppBar_Titleless);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.AppTheme_Widget_CollapsedAppBar);

        initShareButtons();
    }

    private ArrayList<ContactInfoItem> loadContactInfo() {
        ArrayList<ContactInfoItem> detailList = new ArrayList<ContactInfoItem>();

        //phone list
        if (currentTournament.getPhone() != null && currentTournament.getPhone().contains("/")) {

            String[] phonesList = currentTournament.getPhone().split("/");
            int phonesLenght = phonesList.length;
            int i = 1;
            for (String phone: phonesList) {
                ContactInfoItem item = new ContactInfoItem();
                item.setPrimaryTitle(phone.trim());
                item.setSecondaryTitle(getString(R.string.click_to_call));
                item.setPrimaryIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
                item.setType(ContactInfoItem.ItemType.PHONE);

                if (i == phonesLenght) {
                    item.setShowDivider(true);
                } else {
                    item.setShowDivider(false);
                }

                detailList.add(item);

                i++;
            }

        } else {

            ContactInfoItem item = new ContactInfoItem();
            item.setPrimaryTitle(currentTournament.getPhone());
            item.setSecondaryTitle(getString(R.string.click_to_call));
            item.setPrimaryIcon(getResources().getDrawable(android.R.drawable.ic_menu_call));
            item.setType(ContactInfoItem.ItemType.PHONE);
            item.setShowDivider(true);

            detailList.add(item);

        }

        //contact info
        if(currentTournament.getMail() != null) {
            ContactInfoItem item = new ContactInfoItem();
            item.setPrimaryTitle(getString(R.string.contact_info_tournament));
            item.setSecondaryTitle(getString(R.string.click_to_contact));
            item.setPrimaryIcon(getResources().getDrawable(R.drawable.ic_email_black_24px));
            item.setType(ContactInfoItem.ItemType.CONTACT_INFO);
            item.setShowDivider(true);
            item.setEntity(currentTournament);

            detailList.add(item);
        }

        return detailList;
    }

    private ArrayList<Entity> loadHeadquearters() {
        ArrayList<Entity> playfields = new ArrayList<Entity>();

        if(currentTournament.getHosts().size() > 0) {
            for (Headquarter headquearter: currentTournament.getHosts()) {
                if(headquearter.getPlayfield() != null) playfields.add(headquearter.getPlayfield());
            }
        }
        return playfields;
    }

    private void loadInfo() {
        TextView infoTitle = (TextView) findViewById(R.id.info_description_title);
        infoTitle.setText(getString(R.string.info_description_title_tournament));

        String features = "";

        if(currentTournament.getDescription() != null) {
            features += currentTournament.getDescription() + " <br><br>";
        }

        features += "SERVICIOS: <br>";
        if(currentTournament.getFeatures().size() > 0) {
            for(int i=0; i < currentTournament.getFeatures().size(); i++) {
                Feature feature = currentTournament.getFeatures().get(i);
                features += feature.getTitle() + "<br>";
            }
        }

        final int linesCount = linesCount(features);

        final TextView infoDescription = (TextView) findViewById(R.id.info_description);
        infoDescription.setText(Html.fromHtml(features));

        final Button viewMore = (Button) findViewById(R.id.view_more);
        viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewMore.getText().equals(getString(R.string.view_more))) {
                    viewMore.setText(getString(R.string.view_less));
                    infoDescription.setLines(linesCount);
                } else {
                    viewMore.setText(getString(R.string.view_more));
                    infoDescription.setLines(3);
                }
            }
        });
    }

    private int linesCount(String text) {
        String[] lines = text.split("<br>");
        return  lines.length;
    }

    private void loadPositiveVotes() {
        mPrimaryPositiveVotesCount = (TextView) findViewById(R.id.primary_positive_votes_count);
        mSecondaryPositiveVotesCount = (TextView) findViewById(R.id.secondary_positive_votes_count);

        if(currentTournament.getPositiveVotes() != null) {
            mPrimaryPositiveVotesCount.setText(currentTournament.getPositiveVotes().toString());
            mSecondaryPositiveVotesCount.setText(currentTournament.getPositiveVotes().toString());
        }
    }

    private void loadWebSite() {
        if(currentTournament.getWeb() != null) {
            TextView webSite = (TextView) findViewById(R.id.info_website_url);
            webSite.setText(currentTournament.getWeb());

            webSite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String urlSite = "";
                    if(currentTournament.getWeb().contains("http://")) {
                        urlSite = currentTournament.getWeb();
                    } else {
                        urlSite = "http://" + currentTournament.getWeb();
                    }

                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(urlSite));
                    startActivity(i);
                }
            });
        } else {
            View infoWebSiteView = (View) findViewById(R.id.info_website);
            infoWebSiteView.setVisibility(View.GONE);
        }
    }

    private void setupMap() {
        Button showInMap = (Button) findViewById(R.id.view_in_map);
        showInMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TournamentDetailActivity.this, MapActivity.class);
                intent.putExtra("entity", (Serializable) currentTournament);

                startActivity(intent);
            }
        });
    }

    public void initShareButtons() {
        if(mTwitterButton == null) mTwitterButton = (FloatingActionButton) findViewById(R.id.share_twitter_button);
        if(mFacebookButton == null) mFacebookButton = (FloatingActionButton) findViewById(R.id.share_facebook_button);

        mFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inFabebook(TournamentDetailActivity.this, currentTournament.getDetailUrl());
            }
        });

        mTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomSocialShare.inTwitter(TournamentDetailActivity.this, currentTournament.getDetailUrl());
            }
        });

        mFacebookButton.setColorFilter(Color.argb(255, 255, 255, 255));
        mTwitterButton.setColorFilter(Color.argb(255, 255, 255, 255));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_article_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_article_detail_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, currentTournament.getDetailUrl());
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.activity_share_subject));
            startActivity(Intent
                    .createChooser(intent, getResources().getString(R.string.activity_share_title))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(status.equals("success")) {
            currentTournament.setPositiveVotes(currentTournament.getPositiveVotes()+1);

            mPrimaryPositiveVotesCount.setText(currentTournament.getPositiveVotes().toString());
            mSecondaryPositiveVotesCount.setText(currentTournament.getPositiveVotes().toString());
        }

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorOcurred(Exception e) {

    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {

    }

    private void addPositiveVotes() {
        if(mTournamentService == null) mTournamentService = new TournamentService("PositiveVotes", this, this);

        Map<String, String> params = new HashMap<String, String>();
        params.put("id", String.valueOf(currentTournament.getId()));
        params.put("like", "1");

        mTournamentService.post(params);

        ImageView heart = (ImageView) findViewById(R.id.heart);
        AnimatorUtils.heartBeat(heart);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getInstance().trackScreenView("Detalle Torneos - " + currentTournament.getTitle() + " - " + currentTournament.getId());
    }
}