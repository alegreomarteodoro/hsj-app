package com.p3design.hsj.net;

import com.p3design.hsj.app.App;
import com.p3design.hsj.observer.ConnectivityObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 2/9/15.
 */
public class LiveConnectivityManager {

    private static List<ConnectivityObserver> observers = new ArrayList<ConnectivityObserver>();;
    private static LiveConnectivityManager instance;

    public static LiveConnectivityManager getInstance(){

        if (instance == null){
            instance = new LiveConnectivityManager();
        }

        return instance;
    }

    private LiveConnectivityManager(){

    }

    public void addObserver(ConnectivityObserver observer){
        observers.add(observer);
    }

    public void removeObserver(ConnectivityObserver observer){
        observers.remove(observer);
    }

    void notifyConnectionChange(){

        boolean connected = App.getInstance().getConnectivityInfo().isNetworkAvailable();

        for (ConnectivityObserver observer : observers){
            observer.onStatusNetworkChanged(connected);
        }
    }
}