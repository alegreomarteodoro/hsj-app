package com.p3design.hsj.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by omarteodoroalegre on 2/9/15.
 */
public class ConnectivityReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LiveConnectivityManager.getInstance().notifyConnectionChange();
    }
}