package com.p3design.hsj.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by omarteodoroalegre on 16/7/15.
 */
public class Connectivity {

    protected Context context;

    public Connectivity(Context context) {
        this.context = context;
    }

    public boolean isConnectedWifi(){
        NetworkInfo info = getConnectivityManager().getActiveNetworkInfo();

        if (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI){
            return true;
        }
        return false;
    }

    public boolean isConnectedMobile(){
        NetworkInfo info = getConnectivityManager().getActiveNetworkInfo();
        if(info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE){
            return true;
        }
        return false;
    }

    public boolean isNetworkAvailable() {

        NetworkInfo networkInfo = getConnectivityManager().getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private ConnectivityManager getConnectivityManager() {
        return (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

}
