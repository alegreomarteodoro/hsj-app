package com.p3design.hsj.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.adapter.FeaturedTournamentListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.TournamentService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.DynamicView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class FeaturedTournamentListFragment extends Fragment implements ServiceObserver {

    public static final String ARG_POSITION = "position";
    public static final String SERVICE_ID = "FeaturedTournament";

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mGaggeredGridLayoutManager;
    private FeaturedTournamentListAdapter mFeaturedTournamentListAdapter;
    private ArrayList<Entity> mTournaments = new ArrayList<Entity>();

    private TournamentService mTournamentService;
    DynamicView mDynamicView;
    View mConnectionUnavailableLayout;
    View mLoadingLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mDynamicView = App.newFragmentDynamicView(getContext());
        View view = mDynamicView.addMainViewByLayoutId(R.layout.fragment_featured_playfield_list);

        mConnectionUnavailableLayout = mDynamicView.getView(App.ERROR_CONNECTION_LAYOUT);
        mLoadingLayout = mDynamicView.getView(App.LOADING_LAYOUT);

        RelativeLayout.LayoutParams centerHorizontalLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerHorizontalLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        mLoadingLayout.setLayoutParams(centerHorizontalLayoutParams);

        RelativeLayout.LayoutParams centerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        centerLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        //center layouts
        mConnectionUnavailableLayout.setLayoutParams(centerLayoutParams);

        mGaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        mFeaturedTournamentListAdapter = new FeaturedTournamentListAdapter(mTournaments, getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.featured_playfield_list);
        mRecyclerView.setLayoutManager(mGaggeredGridLayoutManager);
        mRecyclerView.setAdapter(mFeaturedTournamentListAdapter);

        Button retry = (Button) mConnectionUnavailableLayout.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTournaments();
            }
        });

        fetchTournaments();

        AdView mAdView = (AdView) view.findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        return mDynamicView.getRootView();
    }

    private void fetchTournaments() {
        if(mTournamentService == null) {
            mTournamentService = new TournamentService(SERVICE_ID, getContext(), this);
            mTournamentService.disabledProgressDialog();
        }

        mTournamentService.get("/destacado/1/order/orden_destacados/limit/6");
    }

    public static FeaturedTournamentListFragment newInstance(int position) {
        FeaturedTournamentListFragment featuredTournamentListFragment = new FeaturedTournamentListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        featuredTournamentListFragment.setArguments(args);
        return featuredTournamentListFragment;
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mTournaments.clear();
            mTournaments.addAll(entities);
            mFeaturedTournamentListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {
        mDynamicView.showView(App.ERROR_CONNECTION_LAYOUT);
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(Service.COMPLETED.equals(status)) {
            mDynamicView.showView(App.MAIN_LAYOUT);
        } else if(Service.LOADING.equals(status)) {
            mDynamicView.showView(App.LOADING_LAYOUT);
        }
    }
}