package com.p3design.hsj.fragment;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.adapter.NearbyTournamentListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.location.LocationTracker;
import com.p3design.hsj.observer.LocationObserver;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.service.TournamentService;
import com.p3design.hsj.util.DynamicView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 17/2/16.
 */
public class NearbyTournamentListFragment extends Fragment implements ServiceObserver, LocationObserver {

    private static final String TAG = NearbyPlayfieldsListFragment.class.getSimpleName();

    public static final String ARG_POSITION = "position";
    public static final String SERVICE_ID = "NearbyTournamentList";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private NearbyTournamentListAdapter mNearbyTournamentListAdapter;
    private ArrayList<Entity> mTournaments = new ArrayList<Entity>();
    private TournamentService mTournamentService;
    LocationTracker mLocationTracker;
    private int mDistance = 50;

    DynamicView mDynamicView;
    View mLocationUnavailableLayout;
    View mConnectionUnavailableLayout;
    View mLoadingLayout;
    View mNoMatchResultsLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mDynamicView = App.newFragmentDynamicView(getContext());
        View view = mDynamicView.addMainViewByLayoutId(R.layout.fragment_nearby_playfield_list);

        mLocationTracker = new LocationTracker(this.getContext(), this);

        mLocationUnavailableLayout = mDynamicView.getView(App.ERROR_LOCATION_LAYOUT);
        mConnectionUnavailableLayout = mDynamicView.getView(App.ERROR_CONNECTION_LAYOUT);
        mNoMatchResultsLayout = mDynamicView.addViewByLayoutId(App.NOT_MATCH_RESULTS_LAYOUT, R.layout.global_no_match_nearby_results);
        mLoadingLayout = mDynamicView.getView(App.LOADING_LAYOUT);

        RelativeLayout.LayoutParams centerHorizontalLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerHorizontalLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        mLoadingLayout.setLayoutParams(centerHorizontalLayoutParams);

        RelativeLayout.LayoutParams centerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        centerLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        //center layouts
        mLocationUnavailableLayout.setLayoutParams(centerLayoutParams);
        mConnectionUnavailableLayout.setLayoutParams(centerLayoutParams);
        mNoMatchResultsLayout.setLayoutParams(centerLayoutParams);


        mNearbyTournamentListAdapter = new NearbyTournamentListAdapter(mTournaments, getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.nearby_playfield_list);
        mLinearLayoutManager = new LinearLayoutManager(this.getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mNearbyTournamentListAdapter);

        Button settings = (Button) mLocationUnavailableLayout.findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLocationSettingsDialog();

            }
        });

        Button retry = (Button) mConnectionUnavailableLayout.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchPlayfields();
            }
        });

        Button searchMoreFar = (Button) mNoMatchResultsLayout.findViewById(R.id.search_more_far);
        searchMoreFar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDistance = mDistance * 2;
                fetchPlayfields();
            }
        });

        if(mLocationTracker.getLastLocation() == null) {
            showLocationUnavailableView();
        }

        AdView mAdView = (AdView) view.findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        return mDynamicView.getRootView();
    }

    public static NearbyTournamentListFragment newInstance(int position) {
        NearbyTournamentListFragment nearbyTournamentListFragment = new NearbyTournamentListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        nearbyTournamentListFragment.setArguments(args);
        return nearbyTournamentListFragment;
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mTournaments.clear();
            mTournaments.addAll(entities);
            mNearbyTournamentListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {
        showConnectionUnavailableView();
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(Service.COMPLETED.equals(status)) {
            if(mTournaments.size() == 0) {
                TextView message = (TextView) mNoMatchResultsLayout.findViewById(R.id.message);

                String msg = getResources().getString(R.string.empty_nearby_tournaments);
                message.setText(String.format(msg, mDistance));

                mDynamicView.showView(App.NOT_MATCH_RESULTS_LAYOUT);
            } else {
                showPlayfieldListView();
            }
        } else if(Service.LOADING.equals(status)) {
            showLoading();
        }
    }

    private void fetchPlayfields() {
        if(mTournamentService == null) {
            mTournamentService = new TournamentService(SERVICE_ID, getContext(), this);
            mTournamentService.disabledProgressDialog();
        }

        Location lastLocacion = mLocationTracker.getLastLocation();
        if(lastLocacion != null) {
            mTournamentService.get("/coordenadas/" + lastLocacion.getLatitude() + "," + lastLocacion.getLongitude() + "/distancia/" + mDistance);
        } else {
            showLocationUnavailableView();
        }
    }

    private void showLoading() {
        mDynamicView.showView(App.LOADING_LAYOUT);
    }

    private void showLocationUnavailableView() {
        mDynamicView.showView(App.ERROR_LOCATION_LAYOUT);
    }

    private void showConnectionUnavailableView() {
        mDynamicView.showView(App.ERROR_CONNECTION_LAYOUT);
    }

    private void showPlayfieldListView() {
        mDynamicView.showView(App.MAIN_LAYOUT);
    }

    private void showLocationSettingsDialog() {
        Location lastLocacion = mLocationTracker.getLastLocation();
        if(lastLocacion == null) {
            mLocationTracker.showSettingsAlert();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        fetchPlayfields();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mTournaments.size() == 0) {
            fetchPlayfields();
        }
    }
}