package com.p3design.hsj.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.p3design.hsj.R;
import com.p3design.hsj.adapter.FeaturedPlayfieldListAdapter;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.observer.ServiceObserver;
import com.p3design.hsj.service.PlayfieldService;
import com.p3design.hsj.service.Service;
import com.p3design.hsj.util.DynamicView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omarteodoroalegre on 21/1/16.
 */
public class FeaturedPlayfieldListFragment extends Fragment implements ServiceObserver {

    public static final String ARG_POSITION = "position";
    public static final String SERVICE_ID = "FeaturedPlayfield";

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mGaggeredGridLayoutManager;
    private FeaturedPlayfieldListAdapter mFeaturedPlayfieldListAdapter;
    private ArrayList<Entity> mPlayfields = new ArrayList<Entity>();

    private PlayfieldService mPlayfieldService;

    DynamicView mDynamicView;
    View mConnectionUnavailableLayout;
    View mLoadingLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mDynamicView = App.newFragmentDynamicView(getContext());
        View view = mDynamicView.addMainViewByLayoutId(R.layout.fragment_featured_playfield_list);

        mConnectionUnavailableLayout = mDynamicView.getView(App.ERROR_CONNECTION_LAYOUT);
        mLoadingLayout = mDynamicView.getView(App.LOADING_LAYOUT);

        RelativeLayout.LayoutParams centerHorizontalLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerHorizontalLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        mLoadingLayout.setLayoutParams(centerHorizontalLayoutParams);

        RelativeLayout.LayoutParams centerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        centerLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        //center layouts
        mConnectionUnavailableLayout.setLayoutParams(centerLayoutParams);

        mGaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        mFeaturedPlayfieldListAdapter = new FeaturedPlayfieldListAdapter(mPlayfields, getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.featured_playfield_list);
        mRecyclerView.setLayoutManager(mGaggeredGridLayoutManager);
        mRecyclerView.setAdapter(mFeaturedPlayfieldListAdapter);

        Button retry = (Button) mConnectionUnavailableLayout.findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchPlayfields();
            }
        });

        AdView mAdView = (AdView) view.findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        fetchPlayfields();

        return mDynamicView.getRootView();
    }

    private void fetchPlayfields() {
        if(mPlayfieldService == null) {
            mPlayfieldService = new PlayfieldService(SERVICE_ID, getContext(), this);
            mPlayfieldService.disabledProgressDialog();
        }

        mPlayfieldService.get("/destacadas/1/order/orden_destacados/limit/6");
    }

    public static FeaturedPlayfieldListFragment newInstance(int position) {
        FeaturedPlayfieldListFragment featuredPlayfieldListFragment = new FeaturedPlayfieldListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        featuredPlayfieldListFragment.setArguments(args);
        return featuredPlayfieldListFragment;
    }

    @Override
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {
        if(entities != null && entities.size() > 0) {
            mPlayfields.clear();
            mPlayfields.addAll(entities);
            mFeaturedPlayfieldListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance) {

    }

    @Override
    public void onErrorOcurred(Exception e) {
        mDynamicView.showView(App.ERROR_CONNECTION_LAYOUT);
    }

    @Override
    public void onServiceStatusChanged(String status, Service currentServiceInstance) {
        if(Service.COMPLETED.equals(status)) {
            mDynamicView.showView(App.MAIN_LAYOUT);
        } else if(Service.LOADING.equals(status)) {
            mDynamicView.showView(App.LOADING_LAYOUT);
        }
    }
}
