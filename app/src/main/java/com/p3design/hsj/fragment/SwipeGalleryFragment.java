package com.p3design.hsj.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.p3design.hsj.R;
import com.p3design.hsj.app.App;
import com.p3design.hsj.entity.Image;

import java.io.Serializable;

/**
 * Created by omarteodoroalegre on 1/9/15.
 */
public class SwipeGalleryFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View swipeView = inflater.inflate(R.layout.fragment_swipe_gallery, container, false);

        Bundle bundle = getArguments();
        Image image = (Image) bundle.getSerializable("image");

        ImageView imageGallery = (ImageView) swipeView.findViewById(R.id.gallery_image);
        TextView subtitleGallery = (TextView) swipeView.findViewById(R.id.subtitle_gallery);

        if(image != null) {
            if(image.getUrl() != null)
                App.getInstance().getUniversalImageLoader().displayImage(image.getUrl(), imageGallery);
            if(image.getSubtitle() != null)
                subtitleGallery.setText(image.getSubtitle());
        }

        return swipeView;
    }

    public static SwipeGalleryFragment newInstance(Image image) {
        SwipeGalleryFragment swipeGalleryFragment = new SwipeGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("image", (Serializable) image);
        swipeGalleryFragment.setArguments(bundle);
        return swipeGalleryFragment;
    }
}