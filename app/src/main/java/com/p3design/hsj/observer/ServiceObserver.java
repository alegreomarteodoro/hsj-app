package com.p3design.hsj.observer;

import com.p3design.hsj.entity.Entity;
import com.p3design.hsj.service.Service;

import java.util.List;

/**
 * Created by omarteodoroalegre on 15/7/15.
 */
public interface ServiceObserver {
    /**
     * Notifica cuando el servicio ejecuta la solicitud al servidor y devuelve el resultado.
     * @param status
     * @param message
     * @param entities Lista de resultados.
     * @param currentServiceInstance La instancia del servicio que se esta ejecutando.
     */
    public void onGetResponse(String status, String message, List<Entity> entities, Service currentServiceInstance);
    public void onPostResponse(String status, String message, List<Entity> entities, Service currentServiceInstance);

    /**
     * Notifica cuando ocurre un error cuando el servicio procesa la solicitud.
     * @param e Excepcion
     */
    public void onErrorOcurred(Exception e);

    /**
     * Notifica cuando cambia el estado del servicio.
     * @param status El estado del servicio
     * @param currentServiceInstance La instancia del servicio que se esta ejecutando.
     */
    public void onServiceStatusChanged(String status, Service currentServiceInstance);
}
