package com.p3design.hsj.observer;

/**
 * Created by omarteodoroalegre on 25/2/16.
 */
public interface SearchParamObserver {
    public void onParamAdded(String key, Object value, String visibleValue);
    public void onParamRemoved(String key);
}