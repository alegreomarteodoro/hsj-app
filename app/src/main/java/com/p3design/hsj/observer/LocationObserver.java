package com.p3design.hsj.observer;

import android.location.Location;

/**
 * Created by omarteodoroalegre on 26/1/16.
 */
public interface LocationObserver {
    public void onLocationChanged(Location location);
    public void onProviderEnabled(String provider);
}
