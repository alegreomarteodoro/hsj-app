package com.p3design.hsj.observer;

/**
 * Created by omarteodoroalegre on 1/9/15.
 */
public interface ConnectivityObserver {
    public void onStatusNetworkChanged(boolean connected);
}
